var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.support;
		_$.CurrentControllerName = "support";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
//		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.support_cordoba,$.support_girona,$.support_granada,$.support_madrid,$.support_marbella,
//														   $.support_santander,$.support_santiago,$.support_segovia,$.support_teruel]);

		for(var i=0;i<Ti.App.Properties.getList('cities',[]).length;i++){
			var support = Alloy.createController('item_support',{name:Ti.App.Properties.getList('cities',[])[i].name}).getView();
				support.info = Ti.App.Properties.getList('cities',[])[i];
				
				support.addEventListener('click', function(){
					var emailDialog = Titanium.UI.createEmailDialog();
					    emailDialog.setToRecipients([this.info.soporte]);
					    emailDialog.setSubject('Contacto soporte '+this.info.name);
					    emailDialog.setMessageBody(JSON.stringify(_$.Session)+JSON.stringify(_$.SupportData));
					    emailDialog.addEventListener('complete', function(e) {					        
					    });
					    emailDialog.open();
					
				});
			core.support.push(support);
			$.support_list_container.add(support);
		}
		this.prepareScreen();
		$.support.open();
	},
	support : [],
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();