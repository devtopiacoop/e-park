Titanium.UI.setBackgroundColor('#000');
 
Ti.Map = require('ti.map');
var parser = require('kml_parser');
var communicator = require('communicator');
var core = 
{
	UI	 : $,
	com : communicator,
	polygons : [],
	map  : null,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_GPS_step2;
		_$.CurrentControllerName = "ticket_GPS_step2";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		core.prepareScreen();
		
		//Create a mapview
		core.map = Titanium.Map.createView({
			    mapType: Titanium.Map.STANDARD_TYPE,
			    region: {latitude:40.4018454332669, longitude:-3.68711849216124,
			             latitudeDelta:0.03, longitudeDelta:0.03},
			    animate:true,
			    regionFit:true,
			    userLocation:true
			});
		core.map.addEventListener('click', function(evt) {
			for(i=0;i<core.polygons.length;i++){
				
			}
		
		});
		
		//Add map to UI
		$.map_block_wrap.add(this.map);
		
		$.ticket_GPS_step2.open();
		core.com.zones.getCoordinates(core.callbackForGetCoordinates);
	},
	
	callbackForGetCoordinates : function(result){
		alert(result.neighborhoods.length);
		
		for (var i=0;i<result.neighborhoods.length;i++){ 
			var polygon = Ti.Map.createPolygon({
					fillColor : result.neighborhoods[i].draw_options.fill_color,
					strokeWidth: 1,
					strokeColor: result.neighborhoods[i].draw_options.stroke_color,
					points : result.neighborhoods[i].vertexes
				});
				
			core.polygons.push(polygon);
			//Add polygon to map	
			core.map.addPolygon(core.polygons[core.polygons.length-1]);
		}
		
		/*
		core.map.addEventListener('regionChanged', function(e) {
			for(var i = 0; i<core.polygons.length; i++){
				if(core.polygons[i].contains(e.longitude,e.latitude)){
					core.polygons[i].fillColor("#3f00");
				}else{
					core.polygons[i].fillColor("#30f0");
				}
			}
		});
		*/
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		//$.menu.init("deposit");
		//$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();