communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	com  : communicator,
	languageSelected : function(language){
		index = Ti.App.Properties.getList('languages_name',[]).indexOf(language);
		ISO = Ti.App.Properties.getList('languages_code',[])[index];
		
		Ti.App.Properties.setString('SETTING_LANGUAGE',ISO);

		Ti.API.info("Setting language to "+language);
		
		//Show restart confirmation
		var dialog = Ti.UI.createAlertDialog({
		    cancel: 1,
		    buttonNames: [_$.Strings.DialogRestart,
		    			  _$.Strings.DialogCancel],
		    message: _$.Strings.DialogConfirmationLanguageChange,
		    title: _$.Strings.DialogConfirmationTitleLanguageChange,
		});
		dialog.addEventListener('click', function(e){
	 	   if (e.index === 0){
		   		Ti.API.info('Language changed');
		   		//_$.CurrentController.close();
				core.com.user.Logout();
		   		_$.Tool.openController('index');
		   }else{
		   		Ti.API.info('Language changed, pending');
		   }
		});
		dialog.show();
	},
	disabled : "",
	init : function(){
		
		//User profile
		$.menu_profile.addEventListener('click', function(){
			_$.Tool.openController('profile');
		});
		
		//Deposits
//		if(core.disabled != "deposit"){
			$.deposits.addEventListener('click', function(){
				_$.Tool.openController('deposit');
			});
//		}else{
//			$.deposits.opacity = 0.5;
//		}
		$.support.addEventListener('click', function(){
			_$.Tool.openController('support');
		});
		
		//Your vehicles
		$.vehicle_list.addEventListener('click', function(){
			_$.Tool.openController('ticket_vehicles');
		});
		
		//Movements
		$.movements.addEventListener('click', function(){
			_$.Tool.openController('movements');
		});
	
		

		
		//Languages
		$.languages.addEventListener('click', function(){
			_$.Tool.openControllerPopUp('autocomplete_input',{callback:core.languageSelected,data:Ti.App.Properties.getList('languages_name',[])});
		});
		
		$.menu_logout.addEventListener('click', function(){
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: [_$.Strings.DialogConfirm,
			    			  _$.Strings.DialogCancel],
			    message: String.format(_$.Strings.DialogConfirmationLogout,_$.Profile.user.name),
			    title: _$.Strings.DialogConfirmationTitleLogout,
			});
			dialog.addEventListener('click', function(e){
		 	   if (e.index === 0){
			   		Ti.API.info('Logout confirmed');
			   		//_$.CurrentController.close();
					core.com.user.Logout();
			   		_$.Tool.openController('home');
			   }else{
			   		Ti.API.info('Logout cancelled');
			   }
			});
			dialog.show();
		});
		
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage('images/main_bg_pattern.png' );
		//Ti.UI.setBackgroundColor("#167A94");
	}
};

exports.init = function(disabled){
	if(disabled) core.disabled = disabled;
	core.init();
};
