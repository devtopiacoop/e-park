/**
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_deposit` we didn't receive data'");
Ti.API.info(JSON.stringify(args));
$.deposit_amount.text = (parseInt(args.Importe)/100).toFixed(2)+"€";
$.deposit_date.text = args.Fecha || "";
$.deposit_card.text = args.card || "";