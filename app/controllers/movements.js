_$ = Alloy.Globals;
communicator = require("communicator");

var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.movements;
		_$.CurrentControllerName = "movements";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("navigation");
			});	
		}
	},
	movements : [],
	callbackForGetCurrentTickets : function(response){
		
			Ti.API.info(JSON.stringify(response));
			if(response.pagos && response.pagos.length > 0){
				//There are movements, so hide this message first
				$.loading_movements.height = 0;
				
				//Add all views
				for(i=0;i<response.pagos.length;i++){
					var movement = Alloy.createController('item_movements',response.pagos[i]).getView();
						//movement.info = response.ticket[i];	
					Ti.API.info("Added a movement to the list.");
				
					core.movements.push(movement);
					$.movement_list_container.add(movement);
				}
				//Add haptic feedback for buttons
				//_$.Tool.setTouchListenersForFeedback($,core.vehicles);
				//$.movement_count.text=String.format("Hay %d tickets activos",response.ticket.length);
				//Ti.API.info(JSON.stringify(response));
			}
		
		
		
	},
	init : function(){
		self = this;
		core.navigation(true);
		core.com.tickets.HistoricOfMovements(core.callbackForGetCurrentTickets);
		$.movements.open();
		
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = function(){Ti.API.info("received event");if(!_$.Tool.menuOpen) core.toggleMenu();};
    	Ti.App.addEventListener('openMenu', _$.om);
    	_$.cm = function(){Ti.API.info("received event");if(_$.Tool.menuOpen) core.toggleMenu();};
		Ti.App.addEventListener('closeMenu',_$.cm);
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();