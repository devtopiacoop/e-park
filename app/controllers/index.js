Ti.UI.setBackgroundImage(null);
_$ = Alloy.Globals;
communicator = require("communicator");

//TODO controller navigation is now BROKEN due to ANDROID
//TODO disable interactions when pending network call, like login 
//TODO wrap menu in slide to prevent overlapping in  landscape mode
//TODO app-wide, when clicking a menu item, close the menu afterwards
//WE NEED TO SORT THIS ASAP
var core = 
{
	UI	 : $,
	com : communicator,
	callbackForLiterals : function(result){
		Ti.API.info(JSON.stringify(result));
	},
	callbackForSetup : function(result){
		if(result.city && result.city.length > 0){
			_$.cities = result.city;
			//Save the available cities for the app
			Ti.App.Properties.setList('cities',result.city);
			Ti.API.info(String.format("Saved cities[%s]",result.city.length+""));
		}
		if(result.color && result.color.length > 0){
			//Save the available colors for the vehicles
			Ti.App.Properties.setList('colors',result.color);
			Ti.API.info(String.format("Saved colors[%s]",result.color.length+""));
			
			colors = [];
			for(i=0;i<result.color.length;i++){
				colors.push(result.color[i].color);
				Ti.API.info("pushing color "+JSON.stringify(result.color[i].color));
			}
			//Save the available color for the app
			Ti.App.Properties.setList('color',colors);
			Ti.API.info(String.format("Saved colors[%s]",colors.length+""));
		}
		if(result.languages && result.languages.length > 0){
			languages_code = [];
			languages_name = [];
			for(i=0;i<result.languages.length;i++){
				languages_name.push(result.languages[i].name);
				languages_code.push(result.languages[i].language);
				Ti.API.info("pushing languages "+JSON.stringify(result.languages[i]));
			}
			//Save the available languages for the app
			Ti.App.Properties.setList('languages_name',languages_name);
			Ti.App.Properties.setList('languages_code',languages_code);
			Ti.API.info(String.format("Saved languages[%s]",result.languages.length+""));
		}
		if(result.countries && result.countries.length > 0){
			//Save the available countries for the app
			Ti.App.Properties.setList('countries',result.countries);
			Ti.API.info(String.format("Saved countries[%s]",result.countries.length+""));
		}
		if(result.marcas && result.marcas.length > 0){
			vendors = [];
			for(i=0;i<result.marcas.length;i++){
				vendors.push(result.marcas[i].marca);
				Ti.API.info("pushing vendor "+JSON.stringify(result.marcas[i]));
			}
			//Save the available brands for the app
			Ti.App.Properties.setList('vendors',vendors);
			Ti.API.info(String.format("Saved vendors[%s]",vendors.length+""));
		}
		
		//Set the firstLaunch boolean to false to avoid loading this every time.
		Ti.API.info("Set the firstLaunch boolean to false to avoid loading this every time");
		Ti.App.Properties.setBool("firstLaunch",true);
	},
	
	navigation : function(){
		_$.PreviousController = _$.CurrentControllerName;
		_$.CurrentController = $.index;
		_$.CurrentControllerName = "index";
	},
	
	init : function(){
		//alert("USER / PASS: 05311349m / 12345");
		self = this;
		Ti.UI.setBackgroundColor('white');
		core.navigation();
		
		//Fetch configuration from servers
		if(true){//Ti.App.Properties.getBool("firstLaunch",true)){
			//This is our first launch, fetch data from server for configuration
			core.com.Setup(self.callbackForSetup);
			//core.com.GetLiterals(self.callbackForLiterals);
		}else{
			Ti.API.info("No need to load configuration from server :)");
		}
		
		_$.Tool.setTouchListenersForFeedback($,[$.splash_enter_wrap]);
		
		$.splash_enter.addEventListener('click', function(){
			_$.Tool.openController('home');
		});
		
		this.prepareScreen();
		$.index.open();
	},
	valueSelectedListener : function(e){
		alert("SELECTED:"+e);
	},
	prepareScreen : function(){
		//Ti.UI.setBackgroundImage('images/main_bg_pattern.png' );
	}
};
core.init();