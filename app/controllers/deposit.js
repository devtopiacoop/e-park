var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.deposit;
		_$.CurrentControllerName = "deposit";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("navigation");
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		
		_$.Tool.setTouchListenersForFeedback($,[$.deposit_historic_wrap,$.deposit_credit_card_wrap,$.deposit_balance_wrap]);

		$.deposit_historic_wrap.addEventListener('click', function(){
			_$.Tool.openController("deposit_list");
		});
		
		$.deposit_credit_card_wrap.addEventListener('click', function(){
			_$.Tool.openController("cards");
		});
		
		$.deposit_balance.text = String.format("Recargar saldo (%s)",(parseInt(_$.Profile.user.userBalance)/100).toFixed(2)+"€");
		$.deposit_balance_wrap.addEventListener('click', function(){
			_$.Tool.openController("balance_add");
		});
		
		this.prepareScreen();
		$.deposit.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = function(){Ti.API.info("received event");if(!_$.Tool.menuOpen) core.toggleMenu();};
    	Ti.App.addEventListener('openMenu', _$.om);
    	_$.cm = function(){Ti.API.info("received event");if(_$.Tool.menuOpen) core.toggleMenu();};
		Ti.App.addEventListener('closeMenu',_$.cm);
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();