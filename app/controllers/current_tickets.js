_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.current_ticket;
		_$.CurrentControllerName = _$.PreviousController;
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("index");
			});	
		}
	},
	init : function(){
		self = this;
		_$.Tool.clearSessionData();
		core.navigation(false);
		
		$.current_tickets.open();
		//_$.CurrentController.close();
		
		$.go_back.addEventListener('click', function(){
			Alloy.Globals.Tool.openController('home',$.current_tickets,true);
		});
		
		
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();