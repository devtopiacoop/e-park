communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		_$.CurrentController = $.balance_add;
		_$.CurrentControllerName = "balance_add";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	formatInput : function(){
		//Capped to 3 digits, for a maximum of 999 (Check with specs)
		$.balance_add_input.value = $.balance_add_input.value.replace(/\D/g,'').substring(0,3);
	},
	callbackForAddBalance : function(result){
		_$.Tool.openController("deposit");
	},
	init : function(){
		self = this;
		core.navigation(true);
		
		//Control the user input for formatting
		$.balance_add_input.addEventListener('change',core.formatInput);
		
		_$.Tool.setTouchListenersForFeedback($,[$.balance_add_ok]);
		
		//Add balance
		$.balance_add_ok.addEventListener('click', function(){
			if($.balance_add_password_input.value != "" && 
			   $.balance_add_input.value != "" &&
			   parseInt($.balance_add_input.value) >= 5){
				core.com.deposits.Add({idTarjeta:parseInt(Ti.App.Properties.getString("card_id","0")),
									   pin:$.balance_add_password_input.value,
									   importe:parseInt($.balance_add_input.value)*100},
									  core.callbackForAddBalance);
			}else{
				alert("Por favor, rellena los campos. El importe mínimo es 5€");
			}
			
		});
		
		this.prepareScreen();
		$.balance_add.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();