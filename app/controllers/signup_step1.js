_$ = Alloy.Globals;
id_validator = require("id_validator");
form_validator = require("form_validator");
var args = arguments[0] || {};

var core = 
{
	UI	 : $,
	validator : id_validator,
	form_validator : form_validator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.signup_step1;
		_$.CurrentControllerName = "signup_step1";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	prefillForm : function(){
		if(args.newUser){
			 $.signup_name_input.value = core.data.nombre    ;
			 $.signup_surname_input.value = core.data.apellidos;
			 $.signup_surname_2_input.value = "" ;
			 $.signup_phone_input.value = core.data.telefono  ;
			 $.signup_email_1_input.value = core.data.email ;
			 $.signup_street_input.value = core.data.calle ;
			 $.signup_number_input.value = core.data.numero ;
			 $.signup_flat_input.value = core.data.piso ;
			 $.signup_postcode_input.value = core.data.cp ;
		}
	},
	validateForm : function(){
		required = core.form_validator.empty([$.signup_city_input,$.signup_city_input,$.signup_dni_input,$.signup_email_2_input,
			 							   	   $.signup_email_1_input,$.signup_name_input,$.signup_phone_input,$.signup_surname_input,
								   	   	   	   $.signup_street_input,$.signup_number_input,$.signup_postcode_input]);
		other = $.signup_email_2_input.value == $.signup_email_1_input.value;
		if(!other){
			$.signup_email_2_input.getParent().borderColor = _$.Colors.BorderInputError;
			$.signup_email_1_input.getParent().borderColor = _$.Colors.BorderInputError;
		}
		id = core.validateId($.signup_dni_input);
		
		Ti.API.info(required.toString()+":"+other.toString()+":"+id.toString());
		
		return(required && other && id);
	},
	validateId : function(field){
		text = field.value;
		 
		if(text.length < 5){
			//Not worth validating
			$.dni_tooltip_container.visible = false;
			field.getParent().borderColor = _$.Colors.BorderInputError;	
			return false;
		}else{
			result = core.validator.validateSpanishID(text);
			Ti.API.info(JSON.stringify(result));
			if(result.valid){
				$.dni_tooltip_container.visible = true;
				field.getParent().borderColor = _$.Colors.BorderInputIdle;
				$.dni_tooltip.text = result.type.toUpperCase();
				core.data.nif = text;
				core.data.documentacion = ["dni","cif","nie"].indexOf(result.type);
				
				Ti.API.info(JSON.stringify(core.data));
				return true;
			}else{
				$.dni_tooltip_container.visible = false;
				field.getParent().borderColor = _$.Colors.BorderInputError;
				return false;
			}	
		}
	},
	data : {},
	init : function(){
		self = this;
		core.navigation(true);
		//_$.Session.Ticket.Zone = undefined; //Clear zone, since we're not in that step anymore
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.signup_next_button]);
		
		$.signup_step1.open();
		core.prefillForm();
		
		$.signup_next_button.addEventListener('click', function(){
			if(core.validateForm()){
				core.data.userAgent = "Web";
				core.data.nombre    = $.signup_name_input.value;
				core.data.ciudad = $.signup_city_input.value;
				core.data.apellidos = $.signup_surname_input.value+" "+$.signup_surname_2_input.value;
				core.data.telefono  = $.signup_phone_input.value;
				core.data.email = $.signup_email_1_input.value;
				core.data.calle = $.signup_street_input.value;
				core.data.numero = $.signup_number_input.value;
				core.data.piso = $.signup_flat_input.value;
				core.data.cp = $.signup_postcode_input.value;

				Alloy.Globals.Tool.openController('signup_step2',{newUser:core.data});	
			}else{
				alert("Por favor, verifica los campos resaltados");
			}
		});
		
		Alloy.Globals.Tool.highlightParents([$.signup_city_input,$.signup_city_input,$.signup_dni_input,$.signup_email_2_input,
			 							   	   $.signup_email_1_input,$.signup_name_input,$.signup_phone_input,$.signup_surname_input,
								   	   	   	   $.signup_street_input,$.signup_number_input,$.signup_postcode_input],
											"#5AAFEE","#E9E9E9");
											
		
		$.signup_dni_input.addEventListener('change',function(){
			core.validateId($.signup_dni_input);
		});
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();