var args = arguments[0] || {};
var core = 
{
	UI	 : $,
	type : {},
	input: [],
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.vehicles_plate_input;
		_$.CurrentControllerName = "vehicles_plate_input";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	createInputFieldsForPlateType : function(){
		core.type = args;
		totalLength = 0;
		
		$.plate_type_detail.text = core.type.detail;
		$.plate_type_example.text = core.type.example;
		
		for(var i=0;i<core.type.boxes.length;i++) totalLength += parseInt(core.type.boxes[i]['length']);
		for(var i=0;i<core.type.boxes.length;i++){
			var licensePlateInput = Alloy.createController('item_vehicle_plate_input',core.type.boxes[i]).getView();
				licensePlateInput.info = core.type.boxes[i];
				licensePlateInput.width = ((parseInt(core.type.boxes[i]['length'])/totalLength*100)-1)+"%";
				licensePlateInput.keyboardType = core.type.boxes[i].type=="N"?Ti.UI.KEYBOARD_NUMBER_PAD:Ti.UI.KEYBOARD_DEFAULT;
				licensePlateInput.maxLength = parseInt(core.type.boxes[i]['length']);
				licensePlateInput.autocapitalization = Ti.UI.TEXT_AUTOCAPITALIZATION_ALL;
				
				licensePlateInput.addEventListener('change',function(e){
					//e.source.value = e.source.value.toUpperCase();
					this.value = e.source.value;
				});
				
				Ti.API.info("Added a license plate input field to the list.");
				core.input.push(licensePlateInput);
				$.input_container.add(licensePlateInput);
		}
		
	},
	validateLicensePlate : function(){
		for(var i=0;i<core.type.boxes.length;i++){
			if(!core.input[i].value){
				Ti.API.info("FAILED because of an empty textfield");
				Ti.API.info(core.type.boxes.length);
				Ti.API.info(core.input[i].value);
				return false;
			}
			if(core.input[i].value.length != parseInt(core.type.boxes[i]['length'])){
				Ti.API.info("FAILED because of the length of field "+i+" should be:"+parseInt(core.type.boxes[i]['length']));
				return false;
			}
			if(/^[a-z0-9]+$/i.test(core.input[i].value)){
				//Either numbers or letters, good start
				if(core.type.boxes[i].type == "N" && !/^[0-9]+$/i.test(core.input[i].value || "")){
					Ti.API.info("FAILED because "+core.input[i].value+" should only contain numbers");
					return false;
				}
				if(core.type.boxes[i].type == "L" && !/^[a-z]+$/i.test(core.input[i].value || "")){
					Ti.API.info("FAILED because "+core.input[i].value+" should only contain letters");
					return false;
				}
			}
		}
		return true;
	},
	init : function(){
		core.navigation(true);
		core.createInputFieldsForPlateType();
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.vehicles_plate_input_accept]);
		
		$.vehicles_plate_input_accept.addEventListener('click', function(){
			out = "";
			for(var i=0;i<core.type.boxes.length;i++) out+=core.input[i].value;
			
			if(core.validateLicensePlate()){
				_$.Session.newVehicle.vehicleRegistration = out;
				//_$.Tool.openController("vehicle_edit");
			}else{
				alert("La matrícula proporcionada no tiene un formato válido");
			}
		});
		
		this.prepareScreen();
		$.vehicles_plate_input.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();