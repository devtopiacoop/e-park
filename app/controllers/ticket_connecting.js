var args = arguments[0] || {};
communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	date : require("date").DateUtils,
	com  : communicator,
	init : function(){
        //Load the "loading" screen
        core.navigation();
        $.transport_logo.image = args.url;

		var ticket_data = {
			idZona : _$.Session.Ticket.Zone.idZone,
			fechaInicio: _$.Session.Ticket.Zone.Times.start,
			fechaFin : _$.Session.Ticket.Zone.Times.end,
			matricula : _$.Session.Ticket.Vehicle.vehicleRegistration,
			idTarjeta : Ti.App.Properties.getInt("card_id",0), 
			importe : _$.Session.Ticket.Zone.Slot.cent,
			pin : Ti.App.Properties.getString("hgnmj","")
		};
		
		Ti.API.info(JSON.stringify(ticket_data));
		core.com.tickets.Create(ticket_data,core.callbackForRequestTicket);
		
		this.prepareScreen();
		$.ticket_connecting.open();
	},
	callbackForRequestTicket : function(result){
		Ti.API.info(JSON.stringify(result));
		if(result == " " || result.status != 1){
			//Blank response, not helpful..redirect to navigation and blame server
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ["Volver"],
			    message: "¡Nos ha sido imposible crear el ticket!",
			    title: "Error",
			});
			dialog.addEventListener('click', function(e){
		 	   if (e.index === 0){
			   		$.ticket_connecting.close();
			   }
			});
			dialog.show();
			
		}else
		if(result.status == 1){
			//We have information, we've registered the ticket, we could show the ad
			_$.CurrentControllerName = "navigation";
			_$.Tool.openController('ads',result);
		}
	},
	navigation : function(goback){
		_$.PreviousController = "navigation";
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_connecting;
		_$.CurrentControllerName = "ticket_connecting";
	},
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();