_$ = Alloy.Globals;
var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_step3;
		_$.CurrentControllerName = "ticket_step3";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		
		//Add button feedback
        $.ticket_receipt_cancel_button.addEventListener('touchstart', function(){this.setOpacity(0.8);});
        $.ticket_receipt_cancel_button.addEventListener('touchend', function(){this.setOpacity(1);});
        $.ticket_receipt_accept_button.addEventListener('touchstart', function(){this.setOpacity(0.8);});
        $.ticket_receipt_accept_button.addEventListener('touchend', function(){this.setOpacity(1);});
        
		//GO back to the navigation page
		$.go_back.addEventListener('click', function(){
			Alloy.Globals.Tool.openController('ticket_step2',$.ticket_step3);
		});

		this.prepareScreen();
		$.ticket_step3.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();