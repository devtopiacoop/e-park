communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	card : {},
	com : communicator,
	callbackForDeleteCard : function(result){
		_$.Tool.openController('cards');
		alert("Se ha eliminado la tarjeta");	
	},
	callbackForCardCreateAuth: function(result){
		Titanium.Platform.openURL(result.url);
	},
	callbackForGetCards : function(cards){
		Ti.API.info("Got card:"+JSON.stringify(cards));
		core.card = cards.creditCard;
		Ti.App.Properties.setInt("card_id",cards.creditCard.cardId);
		
		if(core.card && core.card.cardId != 0){
			Ti.API.info("Card has id [raw]:"+cards.creditCard.cardId);
			Ti.API.info("Card has id [cor]:"+core.card.cardId);
			
			$.no_cards.height = 0;	
			$.card_owner.text = core.card.titular;
			$.card_number.text = core.card.cardNumber;
			$.card_date.text = core.card.cardCaducity;
		
			//Add listeners to delete the card
			_$.Tool.setTouchListenersForFeedback($,[$.card_delete]);
			
			$.card_delete.addEventListener('click', function(){
				//Show deletion confirmation
				var dialog = Ti.UI.createAlertDialog({
				    cancel: 1,
				    buttonNames: [_$.Strings.DialogConfirm,
				    			  _$.Strings.DialogCancel],
				    message: _$.Strings.DialogConfirmationDeleteCard,
				    title: _$.Strings.DialogConfirmationTitleDeleteCard,
				});
				dialog.addEventListener('click', function(e){
			 	   if (e.index === 0){
				   		Ti.API.info('Card delete triggered');
				   		core.com.cards.DeleteCard({idTarjeta:core.card.cardId},core.callbackForDeleteCard);
				   }else{
				   		Ti.API.info('Card delete cancelled');
				   }
				});
				dialog.show();
				
			});
		}else{
			$.cards_wrap.height = 0;	
		}
		
	},
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName == "cards"? _$.PreviousController : "deposit";
		//_$.CurrentController.close();
		_$.CurrentController = $.cards;
		_$.CurrentControllerName = "cards";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		core.com.cards.GetCards(core.callbackForGetCards);
		
		_$.Tool.setTouchListenersForFeedback($,[$.card_delete]);
		$.new_card_button.addEventListener('click',function(){
			core.com.cards.CreateCard(core.callbackForCardCreateAuth);
		});
		
		this.prepareScreen();
		$.cards.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		//Ti.UI.setBackgroundColor(null);
	}
};
core.init();