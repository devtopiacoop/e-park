var args = arguments[0] || {};
communicator = require("communicator");
var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		_$.CurrentController = $.ticket_madrid_zone;
		_$.CurrentControllerName = "ticket_madrid_zone";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	barrio : args.data,
	init : function(){
		Ti.API.info(JSON.stringify(args));
		$.barrio.text = args.data.barrio;
		$.ticket_select_zone_label.text = "Selecciona ahora el tipo de tarifa. Tu matrícula "+_$.Session.Ticket.Vehicle.vehicleRegistration;
		core.navigation(true);
		_$.Session.Ticket.idBarrio = args.data.idBarrio;
		core.com.madrid.getZones({idBarrio:args.data.idBarrio},core.callbackForGetZonesMadrid);
		//Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.ticket_select_zone_green,$.ticket_select_zone_blue,$.ticket_select_zone_red,$.ticket_select_zone_resident]);

		this.prepareScreen();
		$.ticket_madrid_zone.open();
	},
	zones : [],
	callbackForGetZonesMadrid : function(result){		
		if(result.zonas && result.zonas.length > 0){
			for(i=0;i<result.zonas.length;i++){
				var zone = Alloy.createController('item_zone',result.zonas[i]).getView();
					zone.info = result.zonas[i];	
				Ti.API.info("Added a zone to the list.");
			
				//Add listeners to newly created button
				zone.addEventListener('click', function(){
					_$.Session.Ticket.Zone = this.info;
					_$.Session.pendingRefresh = true;
					Ti.API.info("Selected zone:"+JSON.stringify(this.info));
					
					
						 if(this.info.name.indexOf("Azul") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#399CC7";
					else if(this.info.name.indexOf("Verde") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#598612";
					else if(this.info.name.indexOf("Roja") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#9D3B34";
					else if(this.info.name.indexOf("Residente") >= 0)	_$.Session.Ticket.Zone.backgroundColor = "#ABABAB";
					
					core.com.madrid.getAuth(
						{
							 idBarrio: this.info.idBarrio,
						     idZona: this.info.idZone,
						     matricula: _$.Session.Ticket.Vehicle.vehicleRegistration,
						     codeBarrio: this.info.codeBarrio,
						     codeZona: 1
					     },
						core.callbackForGetAuth);
					
				});
				
				
				core.zones.push(zone);
				$.zone_list_container.add(zone);
			}
		}
	},
	callbackForGetAuth : function(result){
		if(result.status != 0){
			_$.Session.Ticket.auth = result.authTicket;
			_$.Session.Ticket.Zone.slots = result.slots;
			_$.Tool.openController("ticket_step2");
		}else{
			alert("Error");
		}
	},
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();