_$ = Alloy.Globals;
var args = arguments[0] || {};
var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_select_zone;
		_$.CurrentControllerName = "ticket_select_zone";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	zones : [],
	populateZones : function(){		
		if(args.zone && args.zone.length > 0){
			for(i=0;i<args.zone.length;i++){
				var zone = Alloy.createController('item_zone',args.zone[i]).getView();
					zone.info = args.zone[i];	
				Ti.API.info("Added a zone to the list.");
			
				//Add listeners to newly created button
				zone.addEventListener('click', function(){
					_$.Session.Ticket.Zone = this.info;
					_$.Session.pendingRefresh = true;
					Ti.API.info("Selected zone:"+JSON.stringify(this.info));
					
					
						 if(this.info.name.indexOf("Azul") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#399CC7";
					else if(this.info.name.indexOf("Verde") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#598612";
					else if(this.info.name.indexOf("Roja") >= 0)		_$.Session.Ticket.Zone.backgroundColor = "#9D3B34";
					else if(this.info.name.indexOf("Residente") >= 0)	_$.Session.Ticket.Zone.backgroundColor = "#ABABAB";
					
					_$.CurrentController.close();
					//_$.Tool.openController(_$.PreviousController);
				});
				
				
				core.zones.push(zone);
				$.zone_list_container.add(zone);
			}
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		core.populateZones();
		$.council_title.text = "Ayuntamiento de "+_$.Session.Ticket.City.name;
		
		Ti.API.info(JSON.stringify(args));
		/*
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.ticket_select_zone_green,$.ticket_select_zone_blue,$.ticket_select_zone_red,$.ticket_select_zone_resident]);

		//Load the "loading" screen
		/*$.ticket_select_zone_blue.addEventListener('click', function(){
			Alloy.Globals.Tool.openController('ticket_connecting',$.ticket_select_zone);
		});
		*/
		this.prepareScreen();
		$.ticket_select_zone.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	}
};
core.init();