_$ = Alloy.Globals;
var core = 
{
	UI	 : $,
	date : require("date").DateUtils,
	init : function(){
		this.prepareScreen();
		$.ticket_done.open();
		
		var now = core.date.now();
		var limit = core.date.parse(_$.Session.Ticket.Zone.Times.end);
		
		$.ticket_limit_time.text = limit.toString("HH:mm");
		$.ticket_total_time.text = "";
		
		$.go.addEventListener('click', function(){
			_$.CurrentControllerName = "home";
			_$.Tool.openController('navigation');
		});
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();