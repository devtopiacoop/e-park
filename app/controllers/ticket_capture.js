var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		_$.CurrentController = $.ticket_capture;
		_$.CurrentControllerName = "ticket_capture";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		
		$.ticket_capture.open();
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();