communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	vehicles : [],
	callbackForLoadCars : function(result){
		if(result.vehicles && result.vehicles.length > 0){
			for(i=0;i<result.vehicles.length;i++){
				var vehicle = Alloy.createController('item_vehicle',result.vehicles[i]).getView();
				vehicle.info = result.vehicles[i];	
				Ti.API.info("Added a vehicles to the list.");
			
				//Add listeners to newly created button
				vehicle.addEventListener('click', function(){
					if(core.selectingVehicle){
						Ti.API.info(JSON.stringify("Current session is:"));
						Ti.API.info(JSON.stringify(_$.Session));
						_$.Session.Ticket = _$.Session.Ticket || {};
						_$.Session.Ticket.Vehicle = this.info;
						_$.Tool.openController('ticket_step1');	
						Ti.API.info(JSON.stringify(_$.Session));
					}else{
						_$.Tool.openController('vehicle_edit',this.info);	
					}
				});
				
				core.vehicles.push(vehicle);
				$.vehicle_list.add(vehicle);
			}
			//Add haptic feedback for buttons
			_$.Tool.setTouchListenersForFeedback($,core.vehicles);
			
		}else{
			//There are no vehicles
		}
	},
	com  : communicator,
	selectingVehicle : false,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		
		if(_$.PreviousController == "ticket_step1"){
			//We are selecting a vehicle for a ticket, clicking on the vehicle should
			//return us to the step1, instead of editing the vehicle information
			core.selectingVehicle = true;
		}
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_vehicles;
		_$.CurrentControllerName = "ticket_vehicles";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				if(core.selectingVehicle){
					//If Madrid, we should handle here going to ticket_madrid maybe TODO
					//_$.Tool.openController("ticket_step1");	
				}else{
					//_$.Tool.openController("navigation");	
				}
			});	
		}
	},
	init : function(){
		core.navigation(true);
		//_$.Tool.clearSessionData();
		
		//This screen needs to fetch data from the servers before anything.
		core.com.car.Get(core.callbackForLoadCars);
		
		//Listen to the new vehicle button
		$.ticket_vehicle_new.addEventListener('click', function(){
			_$.Tool.openController('vehicle_edit',$.ticket_vehicles);
		});

		this.prepareScreen();
		$.ticket_vehicles.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();