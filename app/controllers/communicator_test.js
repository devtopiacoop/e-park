communicator = require("communicator");
var core = 
{
	UI	 : $,
	com  : communicator,
	callbackForLogin: function(result){
		$.console.text = $.console.text+":[IN]"+JSON.stringify(result);
		Alloy.Globals.Config.userToken = result.userToken;
		Ti.API.info("Setting token to "+result.userToken);
	},
	callbackForLogout: function(){
		Alloy.Globals.Config.userToken = -1;
		Ti.API.info("Setting token to -1");
	},
	callbackForProfile: function(result){
		$.console.text = $.console.text+":[IN]"+JSON.stringify(result);
		Ti.API.info("Received the profile information:"+JSON.stringify(result));
	},
	callbackForLoadCars: function(result){
		$.console.text = $.console.text+":[IN]"+JSON.stringify(result);
	},
	callbackForCreateCar: function(result){
		$.console.text = $.console.text+":[IN]"+JSON.stringify(result);
	},
	callbackForDeleteCar: function(result){
		$.console.text = $.console.text+":[IN]"+JSON.stringify(result);
	},
	init : function(){
		self = this;
		this.prepareScreen();
		$.communicator_test.open();
		
		$.communicator_login.addEventListener('click', function(){
			$.console.text = '[OUT]ServiceUser/Login:{"NIF":"05311349m","Pwd":"12345","UserAgent":"Android"}';
			self.com.user.Login("05311349m","12345",self.callbackForLogin);
		});
		
		$.communicator_logout.addEventListener('click', function(){
			$.console.text = '[OUT]ServiceUser/Logout';
			self.com.user.Logout(self.callbackForLogout);
		});
		
		$.communicator_load_cars.addEventListener('click', function(){
			$.console.text = '[OUT]ServiceCar/Get';
			self.com.car.Get(self.callbackForLoadCars);
		});
		
		$.communicator_profile.addEventListener('click', function(){
			$.console.text = '[OUT]ServiceUser/Get';
			if(Alloy.Globals.Config.userToken == -1){
				alert("Please login first");
			}else{
				self.com.user.Profile(self.callbackForProfile);
			}
		});
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();