var args = arguments[0] || {};
communicator = require("communicator");

var core = {
	UI	 : $,
	com  : communicator,
	validateInput : function(){
		oldpassword = $.original_password.value;
		newpassword1 = $.new_password1.value;
		newpassword2 = $.new_password2.value;
		
		return (oldpassword != "" && newpassword1 != "" && newpassword2 != "");
	},
	init : function(){
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.accept]);
		$.password_change_input.open();
		
		$.accept.addEventListener('click', function(){
			if(core.validateInput()){
				core.com.user.ChangePassword($.original_password.value,$.new_password1.value,args.callback);
				$.password_change_input.close();
			}else{
				alert("Por favor, rellena los campos correctamente");
			}
		});
		$.cancel.addEventListener('click', function(){
			$.password_change_input.close();
		});
		
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();