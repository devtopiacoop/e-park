var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		_$.CurrentController = $.ticket_madrid_step2;
		_$.CurrentControllerName = "balance_add";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);

		//$.ticket_step2_money_wrapper.addEventListener('scroll', function(e){
    	//	Ti.API.info("Image Scrolled current page: " + e.currentPage);
		//});
		
		//Add button feedback
        $.ticket_step2_accept.addEventListener('touchstart', function(){
        	//this.bg = this.getBackgroundColor();
            this.setOpacity(0.8);
        });
        $.ticket_step2_accept.addEventListener('touchend', function(){
            this.setOpacity(1);
        });
		this.prepareScreen();
		$.ticket_madrid_step2.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();