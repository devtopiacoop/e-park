/**
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_zone` we didn't receive data'");
Ti.API.info(JSON.stringify(args));
//Set values
$.city.text = args.name||"Ciudad";