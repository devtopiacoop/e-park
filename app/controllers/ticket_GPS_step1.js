_$ = Alloy.Globals;
communicator = require("communicator");

var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_GPS_step1;
		_$.CurrentControllerName = "ticket_GPS_step1";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("ticket_step1");
			});	
		}
	},
	streets : [],
	streetsData : [],
	callbackForGettingStreets: function(result){
		core.streets = [];
		for(var i=0;i<result.calles.length;i++){
			core.streets.push(result.calles[i].calle);
			core.streetsData.push(result.calles[i]);
		}
		
		_$.Tool.openControllerPopUp('autocomplete_input',{callback:core.citySelected,data:core.streets});
	},
	citySelected: function(selected){
		Ti.API.info("Selected:"+selected);
		index = core.streets.indexOf(selected);
		Ti.API.info("The index is:"+index+" with data:"+JSON.stringify(core.streetsData[index]));
		_$.Tool.openController('ticket_madrid_zone',{data:core.streetsData[index]});	
	},
	init : function(){
		self = this;
		core.navigation(true);
		var vehicle = _$.Session.Ticket.Vehicle;
		$.title.text = vehicle.vendor+" "+vehicle.model+" "+vehicle.vehicleRegistration;
		
		$.ticket_2_GPS.addEventListener('click', function(){
			Alloy.Globals.Tool.openController('ticket_GPS_step2',$.ticket_GPS_step1);
		});
		
		$.ticket_2_manual.addEventListener('click', function(){
			core.com.madrid.getStreets(core.callbackForGettingStreets);
		});

		//Open the actual controller
		this.prepareScreen();
		$.ticket_GPS_step1.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();