communicator = require("communicator");
id_validator = require("id_validator");
_$ = Alloy.Globals;



var core = 
{
	UI	 : $,
	com  : communicator,
	validator : id_validator,
	callbackForPasswordRecovery: function(result){
		alert("Recibirás un correo con la nueva contraseña");
	},
	callbackForLogin: function(result){
		Ti.API.info("Received "+JSON.stringify(result));
		if(result.userToken && result.userToken.length > 0){
			_$.Tool.openController('navigation',$.home);
			_$.Config.userToken = result.userToken;
			_$.SupportData = result;
 
			_$.Profile.plain_text_password = $.login_password_input.value; //They made me do it.
			Ti.App.Properties.setString("hgnmj",$.login_password_input.value);
			
			Ti.API.info("Setting token to "+result.userToken);
			Ti.API.info("Storing password as:"+$.login_password_input.value);
			
			//We logged, launch profile fetch data to have it around
			core.com.user.Profile();
		}else{
			$.login_dni_input_wrap.borderColor 		= _$.Colors.BorderInputError;
			$.login_password_input_wrap.borderColor = _$.Colors.BorderInputError;
			
			Titanium.UI.createAlertDialog({
			    title:_$.Strings.DialogErrorTitleGeneric,
			    message:_$.Strings.LoginError
			}).show();
		}
	},
	validateID : function(id){
		if(core.validator.validateSpanishID(id).valid){
			//Save the last valid user that logged in as a pref to avoid typing every time
			Ti.App.Properties.setString("last_id",id);
			Ti.API.info("Valid id:"+id);
			return true;
		}else{
			Ti.API.info("Invalid id:"+id);
			return false;	
		}
	},
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.home;
		_$.CurrentControllerName = "home";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("index");
			});	
		}
	},
	init : function(){
		core.navigation(true);

		_$.Tool.setTouchListenersForFeedback($,[$.login_signup_button,$.login_login_button,$.login_password_recovery_button]);
		
		$.login_dni_input.value = Ti.App.Properties.getString("last_id","");
		
		//Signup
		$.login_signup_button.addEventListener('click', function(){
			_$.Tool.openController('signup_step1');
		});
		
		//Login
		$.login_login_button.addEventListener('click', function(){
			if(core.validateID($.login_dni_input.value)){
				data = {NIF:$.login_dni_input.value,Pwd:$.login_password_input.value,UserAgent:"Mobile"};
				core.com.user.Login(data,core.callbackForLogin);	
			}else{
				alert("Por favor, introduce tu DNI/NIE");
			}	
		});
		
		//Recovery password
		$.login_recover_password.addEventListener('click',function(){
			if(core.validateID($.login_dni_input.value)){
				Ti.API.info("Attempting password recovery:"+$.login_dni_input.value);
				core.com.user.RecoverPassword($.login_dni_input.value,core.callbackForPasswordRecovery);
			}else{
				alert("Por favor, introduce tu DNI/NIE");
			}	
		});
		
		//Prepare textviews for decent highlighting
		_$.Tool.highlightParent($.login_dni_input,_$.Colors.BorderInputHighlight,_$.Colors.BorderInputIdle);
		_$.Tool.highlightParent($.login_password_input,_$.Colors.BorderInputHighlight,_$.Colors.BorderInputIdle);
		
		this.prepareScreen();
		$.home.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		//Ti.UI.setBackgroundColor(null);
	}
};
core.init();