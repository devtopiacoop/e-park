var args = arguments[0] || {};
communicator = require("communicator");
_$ = Alloy.Globals;
self = null;

var core = 
{
	UI	 : $,
	com  : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.profile;
		_$.CurrentControllerName = "profile";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	callbackForUpdateUser : function(result){
		alert("Datos actualizados correctamente");
	},
	callbackPasswordChanged : function(result){
		Ti.API.info("Password changed");
	},
	init : function(){
		self = this;
		core.navigation(true);
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.profile_save_changes]);
		
		this.populateInputFields();
		$.profile.open();
		_$.CurrentController = $.profile;
		
		Alloy.Globals.Tool.highlightParents([$.signup_city_input,$.signup_dni_input,
											$.signup_email_1_input,$.signup_name_input,$.signup_phone_input,
											$.signup_surname_input,$.signup_postcode_input],
											"#5AAFEE","#E9E9E9");
		
		$.profile_change_password.addEventListener('click', function(){
			_$.Tool.openControllerPopUp('password_change_input',{callback:core.callbackPasswordChanged});
		});
		
		$.profile_save_changes.addEventListener('click',function(){
			core.com.user.Update(_$.Profile.user,core.callbackForUpdateUser);
		});
		
		this.prepareScreen();
	},
	populateInputFields : function(){
		/*
		 * returned  User : {address,city,cp,email,flat,homeNumber,mailToken,name,nif,phone,
		 * 					 surName,userBalance,userToken,vehicles}
		 */
		$.signup_city_input.value = _$.Profile.user.city;
		$.signup_city_input.addEventListener('change',function(e){
			_$.Profile.user.city = e.source.value;
		});
		
		$.signup_name_input.value = _$.Profile.user.name;
		$.signup_name_input.addEventListener('change',function(e){
			_$.Profile.user.name = e.source.value;
		});
		
		$.signup_dni_input.value = _$.Profile.user.nif;
		$.signup_dni_input.addEventListener('change',function(e){
			_$.Profile.user.nif = e.source.value;
		});
		
		$.signup_surname_input.value = _$.Profile.user.surName;
		$.signup_surname_input.addEventListener('change',function(e){
			_$.Profile.user.surName = e.source.value;
		});
		
		$.signup_phone_input.value = _$.Profile.user.phone;
		$.signup_phone_input.addEventListener('change',function(e){
			_$.Profile.user.phone = e.source.value;
		});
		
		$.signup_email_1_input.value = _$.Profile.user.email;
		$.signup_email_1_input.addEventListener('change',function(e){
			_$.Profile.user.email = e.source.value;
		});
		
		
		$.signup_postcode_input.value = _$.Profile.user.cp;
		$.signup_postcode_input.addEventListener('change',function(e){
			_$.Profile.user.cp = e.source.value;
		});
		
	},
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();