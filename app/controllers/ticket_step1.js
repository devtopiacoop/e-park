_$ = Alloy.Globals;
var core = 
{
	UI	 : $,
	city_names : [],
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_step1;
		_$.CurrentControllerName = "ticket_step1";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("navigation");
			});	
		}
	},
	citySelected : function(result){
		index = core.city_names.indexOf(result);
		_$.Session.Ticket.City = Ti.App.Properties.getList('cities',[])[index];
		
		if(_$.Session.Ticket.City.cityId == 28){//Madrid
			Ti.API.info("Hiding capture block, since madrid is chosen"); 
			$.capture_ticket.visible = false;
		}else{
			Ti.API.info("Showing capture block, since madrid is not chosen"); 
			$.capture_ticket.visible = true;
		}
		
		core.populateData();
		Ti.API.info("RESULT:"+result);
		Ti.API.info("INDEX:"+index);
		Ti.API.info(JSON.stringify(_$.Session.Ticket.City));
	},
	populateData : function(){
		Ti.API.info(JSON.stringify("Current session is:"));
		Ti.API.info(JSON.stringify(_$.Session));
		fail = true;
		if(_$.Session.Ticket){
			fail = false;
			if(_$.Session.Ticket.Vehicle && _$.Session.Ticket.Vehicle.vendor && _$.Session.Ticket.Vehicle.model){
				$.ticket_1_vehicle_label.text = String.format("%s - %s",_$.Session.Ticket.Vehicle.vendor,_$.Session.Ticket.Vehicle.model);
			}else{
				fail = true;
			}
			if(_$.Session.Ticket.City && _$.Session.Ticket.City != ""){
				$.ticket_1_city_label.text = _$.Session.Ticket.City.name;
			}else{
				fail = true;
			}
		}
		if(!fail){
			$.ticket_1_continue_button.addEventListener('click', function(){
				if(_$.Session.Ticket.City.cityId == 28){//Madrid
					_$.Tool.openController('ticket_GPS_step1');
				}else{
					_$.Tool.openController('ticket_step2');
				}
			});
			$.ticket_1_continue_button.opacity = 1;
		}else{
			$.ticket_1_continue_button.opacity = 0.5;
		}
	},
	init : function(){
		core.populateData();
		core.navigation(true);
		_$.Tool.setTouchListenersForFeedback($,[$.ticket_1_vehicle,$.ticket_1_city,$.ticket_1_continue_button]);

		//Choosing a vehicle for the ticket
		$.ticket_1_vehicle.addEventListener('click', function(){
			_$.Tool.openController('ticket_vehicles');
		});
		
		//List the times user have been sued
		$.ticket_1_sued_button.addEventListener('click', function(){
			_$.Tool.openController('ticket_sued');
		});

		//Locating vehicle
		$.ticket_1_city.addEventListener('click', function(){
			//Alloy.Globals.Tool.openController('ticket_GPS_step1',$.ticket_step1);
			//_$.Tool.openController('ticket_step2',$.ticket_step1);
			core.city_names = [];
			for(var i=0;i<Ti.App.Properties.getList('cities',[]).length;i++){
				core.city_names.push(Ti.App.Properties.getList('cities',[])[i].name);
			}
			
			_$.Tool.openControllerPopUp('autocomplete_input',{callback:core.citySelected,data:core.city_names});
		});
		
		this.prepareScreen();
		$.ticket_step1.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();