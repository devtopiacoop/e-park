/**
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_vehicle_plate_type` we didn't receive data'");
Ti.API.info(JSON.stringify(args));
$.vehicle_plate_name.text = args.detail || "";
$.vehicle_plate_example.text = args.example || "";