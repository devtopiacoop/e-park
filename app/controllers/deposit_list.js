communicator = require("communicator");
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.deposit_list;
		_$.CurrentControllerName = "deposit_list";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	deposits : [],
	callbackForGetDeposits : function(result){
		if(result.recargas && result.recargas.length > 0){
			for(i=0;i<result.recargas.length;i++){
				var deposit = Alloy.createController('item_deposit',result.recargas[i]).getView();
				deposit.info = result.recargas[i];	
				Ti.API.info("Added a deposit to the list.");
			
				//Add listeners to newly created button
				/*vehicle.addEventListener('click', function(){
					if(core.selectingVehicle){
						_$.Session.Ticket = _$.Session.Ticket || {};
						_$.Session.Ticket.Vehicle = this.info;
						_$.Tool.openController('ticket_step1');	
					}else{
						_$.Tool.openController('vehicle_edit',this.info);	
					}
				});
				*/
				
				core.deposits.push(deposit);
				$.deposit_list_container.add(deposit);
			}
			//Add haptic feedback for buttons
			//_$.Tool.setTouchListenersForFeedback($,core.vehicles);
			
		}else{
			//There are no deposits
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		
		//This screen needs to fetch data from the servers before anything.
		core.com.deposits.Get(core.callbackForGetDeposits);
				
		$.deposit_list.open();
		
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();