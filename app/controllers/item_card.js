/**
 * NOT USED SINCE ONLY ONE CREDIT CARD IS ALLOWED AT ANY GIVEN TIME
 * 
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_card` we didn't receive data'");
Ti.API.info(JSON.stringify(args));

$.card_owner.text = args.titular;
$.card_number.text = args.cardNumber;
$.card_date.text = args.cardCaducity;