var args = arguments[0] || {};
communicator = require("communicator");
_$ = Alloy.Globals;
self = null;

var core = 
{
	UI	 : $,
	com  : communicator,
	vehicle: {model:"",vendor:"",color:"",vehicleRegistration:""},
	populateInputFields : function(vehicle){
		$.header_text.text = String.format("%s - %s",vehicle.vendor,vehicle.model);
		if(vehicle.color) $.vehicle_color.text = vehicle.color;
		if(vehicle.vendor)$.vehicle_vendor.text = vehicle.vendor;
		if(vehicle.model) $.vehicle_model.value = vehicle.model;
		if(vehicle.vehicleRegistration) $.vehicle_registration.text = vehicle.vehicleRegistration;
	},
	callbackForDeleteCar : function(result){
		_$.Tool.openController('ticket_vehicles');
	},
	callbackForCreateCar : function(result){
		Alloy.Globals.Session.newVehicle = null;
		_$.Tool.openController('ticket_vehicles');
	},
	vendorSelected : function(e){
		$.vehicle_vendor.text = e.toString();
		core.vehicle.vendor = e.toString();
	},
	colorSelected : function(e){
		$.vehicle_color.text = e.toString();
		core.vehicle.color = Ti.App.Properties.getList('color',[]).indexOf(e);
	},
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.vehicle_edit;
		_$.CurrentControllerName = "vehicle_edit";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				//_$.CurrentController.close();
				_$.Tool.openController("ticket_vehicles");
			});	
		}
	},
	init : function(){
		self = this;
		core.navigation(true);
		//Ti.API.info(JSON.stringify(args));
				
		if(args.vehicleRegistration){
			//We are editing a vehicle
			$.save_vehicle.height = 0;
			
			core.vehicle = args;
			this.populateInputFields(core.vehicle);
			$.vehicle_model.editable = false;
			
			$.delete_vehicle.addEventListener('click', function(){
				var dialog = Ti.UI.createAlertDialog({
				    cancel: 1,
				    buttonNames: [_$.Strings.DialogConfirm,
				    			  _$.Strings.DialogCancel],
				    message: String.format(_$.Strings.DialogConfirmationVehicleDelete,core.vehicle.vehicleRegistration),
				    title: _$.Strings.DialogConfirmationTitleVehicleDelete,
				});
				dialog.addEventListener('click', function(e){
			 	   if (e.index === 0){
				   		Ti.API.info('Vehicle deletion confirmed');
				   		_$.Tool.openController('ticket_vehicles',_$.CurrentController);
				   		self.com.car.Delete(core.vehicle,core.callbackForDeleteCar);
				   }else{
				   		Ti.API.info('Vehicle deletion cancelled');
				   }
				});
				dialog.show();
			});
		}else{
			if(_$.Session.newVehicle){
				Ti.API.info("Still editing a new car");
				core.vehicle = _$.Session.newVehicle;
				this.populateInputFields(core.vehicle);	
			}
			
			//We are adding a vehicle
			$.header_text.text = "Nuevo vehículo";
			$.delete_vehicle.height = 0;
			
			$.save_vehicle.addEventListener('click',function(){
				core.com.car.Create(core.vehicle.vehicleRegistration, 
									core.vehicle.vendor, 
									core.vehicle.model, 
									core.vehicle.color, 
									core.callbackForCreateCar);
			});
			
			
			$.vehicle_vendor.addEventListener('click', function(){
				_$.Tool.openControllerPopUp('autocomplete_input',{callback:self.vendorSelected,data:Ti.App.Properties.getList('vendors',[])});
				_$.Session.newVehicle = core.vehicle;
			});
			
			$.vehicle_model.addEventListener('change', function(){
				core.vehicle.model = $.vehicle_model.value;
				_$.Session.newVehicle = core.vehicle;
			});
			
			$.vehicle_color.addEventListener('click', function(){
				_$.Tool.openControllerPopUp('autocomplete_input',{callback:self.colorSelected,data:Ti.App.Properties.getList('color',[])});
				_$.Session.newVehicle = core.vehicle;
			});
			
			$.vehicle_registration.addEventListener('click', function(){
				_$.Tool.openControllerPopUp('vehicles_plate_type');
				_$.Session.newVehicle = core.vehicle;
			});
			
		}

		this.prepareScreen();
		$.vehicle_edit.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();