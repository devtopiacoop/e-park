var core = 
{
	UI	 : $,
	licensePlateTypes : [],
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.vehicles_plate_type;
		
		_$.CurrentControllerName = "vehicles_plate_type";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController);
			});	
		}
	},
	populatePlateTypes : function(){
		Ti.API.info("Generating license type list");
		var countries = Ti.App.Properties.getList('countries',[]);
		
		//We are assuming there's only one country TODO if it goes international, modify here.
		if(countries.length > 0){
			var targetCountry = countries[0];
			for(var i=0;i<targetCountry.formats.length;i++){
				var licensePlateType = Alloy.createController('item_vehicle_plate_type',targetCountry.formats[i]).getView();
				licensePlateType.info = targetCountry.formats[i];
				
				Ti.API.info("Added a license plate type to the list.");
				
				//Add listeners to newly created button
				licensePlateType.addEventListener('click', function(){
					_$.Tool.openController('vehicles_plate_input', this.info);
				});
				
				core.licensePlateTypes.push(licensePlateType);
				$.vehicles_plate_type_list.add(licensePlateType);
			}
			
		}
	},
	init : function(){
		core.populatePlateTypes();
		core.navigation(true);

		this.prepareScreen();
		$.vehicles_plate_type.open();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();