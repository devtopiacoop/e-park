/**
 * NOT TO BE USED ON ITS OWN (!)
 * This class expects to receive an array of values to be presented to the user
 * in a vertical list, and the controller to which it should notify when the user
 * selected a value. If no value is selected, the callback will be called with `null`
 * 
 * author: juan.cortes@devtopia.coop
 */

var args = arguments[0] || {};
var core = {
	UI	 : $,
	data : args.data,
	filter: function(e){return e.toLowerCase().indexOf(value.toLowerCase())>=0;},
	createRow : function(data,clickable){
		var row = Ti.UI.createLabel();
        row.height = 40;
        row.width = Ti.UI.FILL;
        row.font = {fontSize:15,fontFamily: "Lato-Regular"};
        row.color = "#6E6E6E";
        row.textAlign = Ti.UI.TEXT_ALIGNMENT_CENTER;
        
        row.text = data;
        
        if(clickable){
        	//Feedback for touch
	        row.backgroundColor = "#FFFFFF";
	        row.color = "#6E6E6E";
	        row.pressedBackgroundColor = "#F3F3F3";
	        
			child = row;
			child.addEventListener('touchstart', function(){
				this.originalBackground = this.backgroundColor;
				this.backgroundColor = this.pressedBackgroundColor;
			});
        	child.addEventListener('touchend', function(){
				this.backgroundColor = this.originalBackground;
        	});
        	
        	//Select row
        	row.addEventListener("click", function(){
				args.callback(this.text);
        		$.autocomplete_input.close();
        	});
        }
        
        return row;
	},
	onChange: function(){
		core.empty();
		value = $.filtering_text.value;
		values = core.data.filter(core.filter).slice(0,25);
		
		var contentcontainer = Ti.UI.createView();
			contentcontainer.layout = "vertical";
			contentcontainer.height = Ti.UI.SIZE;
			
		if(values.length > 0){
			//There are one or more values, add listeners and views
		    for(i=0;i<values.length;i++){
		    	contentcontainer.add(core.createRow(values[i],true));
		    }
		}else{
			//There are no values, add a dummy row with no listeners
			contentcontainer.add(core.createRow("No hay resultados",false));
		}
		
		$.container.add(contentcontainer);
		contentcontainer = null;
	},
	init : function(){
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.cancel]);
		
		$.filtering_text.addEventListener("change",core.onChange);
		$.autocomplete_input.open();
		core.onChange();
		
		$.cancel.addEventListener('click', function(){
			$.autocomplete_input.close();
		});
		
		this.prepareScreen();
	},
	
	empty : function(){
	    var children = $.container.getChildren();
	    for (var b in children){
	        if (undefined!==children[b]){
	            $.container.remove(children[b]);
	        }
	    }
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();