var args = arguments[0] || {};
_$ = Alloy.Globals;

var core = 
{
	UI	 : $,
	navigation : function(goback){
		_$.PreviousController = "navigation";
		//_$.CurrentController.close();
		_$.CurrentController = $.ads;
		_$.CurrentControllerName = "ads";
	},
	init : function(){
		core.navigation();
		
		$.ads.open();
		$.ad.image = args.url;
		$.ad.addEventListener('click',function(){
			Ti.API.info("AD clicked, close this, return to ticket list");
			Titanium.Platform.openURL(args.urlPublicidad); 
			_$.Tool.openController("ticket_done");
		});
		$.close_ad.addEventListener('click',function(){
			Ti.API.info("AD NOT clicked, close this");
			_$.Tool.openController("ticket_done");
		});
		
		//Common, listen to the menu TODO add the swipe gesture?
		/*$.open_menu.addEventListener('click', function(){
			Alloy.Globals.Tool.menuOpen =!Alloy.Globals.Tool.menuOpen;
			$.sidebar_container.width	= Alloy.Globals.Tool.menuOpen?    Alloy.Globals.Config.sideBarWidth:"0dp";
			$.sidebar_container.opacity	= Alloy.Globals.Tool.menuOpen?    1:0;
			$.content_container.left	= Alloy.Globals.Tool.menuOpen?"-"+Alloy.Globals.Config.sideBarWidth:"0dp";
		});*/
		
		$.ads.addEventListener+
		this.prepareScreen();
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();