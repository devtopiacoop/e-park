_$ = Alloy.Globals;
communicator = require("communicator");

var core = 
{
	UI	 : $,
	com : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.navigation;
		_$.CurrentControllerName = "navigation";
		
		if(goback){
			//Go Back
			/*$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("index");
			});	
			*/
		}
	},
	init : function(){
		$.navigation.addEventListener('androidback' , function(e){
		    var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: [_$.Strings.DialogConfirm,
			    			  _$.Strings.DialogCancel],
			    message: String.format(_$.Strings.DialogConfirmationLogout,_$.Profile.user.name),
			    title: _$.Strings.DialogConfirmationTitleLogout,
			});
			dialog.addEventListener('click', function(e){
		 	   if (e.index === 0){
			   		Ti.API.info('Logout confirmed');
			   		//_$.CurrentController.close();
					core.com.user.Logout();
					$.navigation.close();
			   }else{
			   		Ti.API.info('Logout cancelled');
			   }
			});
			dialog.show();
		});
		
		_$.Tool.clearSessionData();
		Alloy.Globals.Session.Ticket = {};
		core.navigation(false);
		_$.Tool.setTouchListenersForFeedback($,[$.content_1,$.content_2,$.content_3]);
		$.navigation.open();
	
	
		//Set the open ticket count
		//$.nav_ticket_count.text = String.format(_$.Strings.Navigation.TicketCount,Ti.App.Properties.getInt("ticketCount",0));
		
		$.content_1.addEventListener('click', function(){
			Ti.API.info("Navigating to ticket purchase");
			_$.Tool.openController('ticket_step1');
		});
		
		$.content_2.addEventListener('click', function(){
			Ti.API.info("Navigating to active tickets");
			_$.Tool.openController('ticket_active');	
		});
		
		core.prepareScreen();
		
		_.each($.content_container.children, function(child) {
			Ti.API.info(child.className);
		    if (child.className === 'btn') {
		        child.addEventListener('touchstart', function(){this.setOpacity(0.8);});
        		child.addEventListener('touchend', function(){this.setOpacity(1);});
		    }
		});
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = function(){Ti.API.info("received event");if(!_$.Tool.menuOpen) core.toggleMenu();};
    	Ti.App.addEventListener('openMenu', _$.om);
    	_$.cm = function(){Ti.API.info("received event");if(_$.Tool.menuOpen) core.toggleMenu();};
		Ti.App.addEventListener('closeMenu',_$.cm);
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();