var args = arguments[0] || {};
communicator = require("communicator");

var core = 
{
	UI	 : $,
	com  : communicator,
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.signup_step2;
		_$.CurrentControllerName = "signup_step2";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController(_$.PreviousController,{newUser:args.newUser});
			});	
		}
	},
	callbackForUserCreate : function(result){
		Ti.API.info("Creating user result:");
		Ti.API.info(result);
		_$.Tool.openController('home');
	},
	init : function(){
		self = this;
		core.navigation(true);
		//Add highlights to buttons and textfields
		Alloy.Globals.Tool.setTouchListenersForFeedback($,[$.signup_2_complete_button]);
		Alloy.Globals.Tool.highlightParents([$.signup_2_password_input,$.signup_2_repeat_password_input],
											"#5AAFEE","#E9E9E9");
											
		$.signup_step2.open();
		
		$.signup_2_complete_button.addEventListener('click', function(){
			if( $.signup_2_password_input.value != "" &&
				$.signup_2_password_input.value.length > 4 &&
				$.signup_2_repeat_password_input.value != "" &&
				$.signup_2_repeat_password_input.value.length > 4 &&
				$.signup_2_repeat_password_input.value == $.signup_2_repeat_password_input.value){
					//Alloy.Globals.Tool.openController('navigation',$.signup_step2,true);
					args.newUser.pwd = $.signup_2_repeat_password_input.value;
					args.newUser.pwdMD5 = "";
					core.com.user.CreateUpdate(args.newUser,core.callbackForUserCreate);
				}else{
					alert("Las contraseñas han de ser iguales y tener 5 o más caracteres");
				}
		});
		
		$.signup_2_tos_check.on = function() {
		    this.backgroundColor = '#FFF';
		    this.title='✔';
		    this.value = true;
		};
		 
		$.signup_2_tos_check.off = function() {
		    this.backgroundColor = '#EEE';
		    this.title='';
		    this.value = false;
		};
		 
		$.signup_2_tos_check.addEventListener('click', function(e) {
		    if(false == e.source.value) {
		        e.source.on();
		    } else {
		        e.source.off();
		    }
		});
	},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
	}
};
core.init();