/**
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_movements` we didn't receive data'");
Ti.API.info(JSON.stringify(args));

$.movement_date.text = (""+args.Inicio).substring(0,10) || "";
$.movement_start.text = (""+args.Inicio).substring(10) || "";
$.movement_end.text = (""+args.Fin).substring(10) || "";
$.movement_city.text = args.City.name || "";
$.movement_cash.text = (parseInt("0"+args.Importe)/100).toFixed(2)+" €";
$.movement_plate.text = args.Matricula || "";
if(args.ZoneColor){
	$.stripe.backgroundColor = "#"+args.ZoneColor.substring(2);
}
