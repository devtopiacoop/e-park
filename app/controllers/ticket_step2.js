communicator = require("communicator");
_$ = Alloy.Globals;
//TODO saldo/balance not reflected correctly (?)
//fees, slots, money reel not painted (not a UI issue since default values do get painted(?))
//zone not updating when returning, create a callback(?)

var core = 
{
	UI	 : $,
	council:{},
	date : require("date").DateUtils,
	com : communicator,
	slots : [],
	callbackForGetZones : function(result){
		core.council = result;
		$.ticket_balance.text = (parseInt("0"+core.council.userBalance)/100).toFixed(2)+" €";
		$.ticket_plate.text = _$.Session.Ticket.Vehicle.vehicleRegistration;
		$.ticket_step2_zone_id.image=core.council.coatUrl;
	},
	
	navigation : function(goback){
		_$.PreviousController = _$.CurrentControllerName;
		//_$.CurrentController.close();
		_$.CurrentController = $.ticket_step2;
		_$.CurrentControllerName = "ticket_step2";
		
		if(goback){
			//Go Back
			$.go_back.addEventListener('click', function(){
				_$.CurrentController.close();
				//_$.Tool.openController("ticket_step1");
			});	
		}
	},
	getZones : function(){
		data = {};
		data.idCiudad = _$.Session.Ticket.City.cityId;
		data.idResidente = 0;
		data.fecha = ""; //TODO ¿?
		
		core.com.tickets.GetZones(data,core.callbackForGetZones);
	},
	currentSlot : {},
	pendingRefresh : null,
	init : function(){
		self = this;
		core.navigation(true);
		core.pendingRefresh = setInterval(function(){
			if(_$.Session.pendingRefresh){
				core.addValidFees();
				_$.Session.pendingRefresh = false;
				//clearInterval(core.pendingRefresh);
			} 
		},100);
		
		//Set clock to current time
		var d = new Date();
		
		$.ticket_step2_limit_date.text = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
		$.ticket_date.text = d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
		$.ticket_step2_limit_time.text = ("0" + d.getHours()).substr(-2)+":"+("0" + d.getMinutes()).substr(-2);
		$.ticket_time.text = ("0" + d.getHours()).substr(-2)+":"+("0" + d.getMinutes()).substr(-2);
		
		if(_$.Session.Ticket.Extend){
			_$.Session.Ticket = _$.Session.Ticket.Extend;
			core.disabledChangeZone;
		}
		
		core.getZones();
		council_text = "Ayuntamiento de "+_$.Session.Ticket.City.name;
		council_text = council_text.substring(0,27)+(council_text.length>27?"…":"");
		$.council_title.text = council_text;
		
		if(!_$.Session.Ticket.Zone){
			$.ifzoneselected.opacity = 0.7;
			$.ticket_select_zone.height = Ti.UI.SIZE;
			$.ticket_change_zone.height = 0;
			$.ticket_select_zone.addEventListener('click', function(){
				_$.Tool.openController("ticket_select_zone",core.council);
			});
		}else{
			core.addValidFees();
		}
		
		$.ticket_step2_money_wrapper.addEventListener('scroll',core.scrollEventListener);
		core.scrollEventListener();
        
		this.prepareScreen();
		$.ticket_step2.open();
	},
	addValidFees : function(){
		
			$.ticket_change_zone.height = Ti.UI.SIZE;
			$.ticket_select_zone.height = 0;
			$.ticket_change_zone.addEventListener('click', function(){
				_$.Tool.openController("ticket_select_zone",core.council);
			});
			$.ticket_step2_zone_id.image=core.council.coatUrl;
			//We have a zone selected. Change the ticket select zone button
			$.ifzoneselected.opacity = 1;
			$.ticket_change_zone_label.text = _$.Session.Ticket.Zone.name;
			Ti.API.info(JSON.stringify(_$.Session.Ticket.Zone));
			
			if(_$.Session.Ticket.Zone.backgroundColor){
				$.ticket_change_zone.backgroundColor = _$.Session.Ticket.Zone.backgroundColor;	
			}
			$.ticket_step2_charts.addEventListener('click', function(){
	        	_$.Tool.openController("ticket_chart",{url:core.council.feeUrl});
	        });
	        
	        
	        //Clean the dummy fees
			if ($.ticket_step2_money_wrapper && $.ticket_step2_money_wrapper.children != undefined){    
			    // Save childrens        
			    var removeData = [];
			    for (i = $.ticket_step2_money_wrapper.children.length; i > 0; i--){
			        removeData.push($.ticket_step2_money_wrapper.children[i - 1]);  
			    };
			
			    // Remove childrens
			    for (i = 0; i < removeData.length; i++){
			        $.ticket_step2_money_wrapper.remove(removeData[i]);
			    }
			    removeData = null;
			};
			
			//Depending on the current time, choose a price slot or another TODO
			Ti.API.info(JSON.stringify(_$.Session.Ticket.Zone.slots));
			/*
			for(var i=0;i<_$.Session.Ticket.Zone.slots.length;i++){
				Ti.API.info("INSIDE");
				var currentTime = core.date.now();
				var startTime = core.date.parse(_$.Session.Ticket.Zone.slots[i].startTime);
				var endTime = core.date.parse(_$.Session.Ticket.Zone.slots[i].endTime);
				
				Ti.API.info(startTime);
				Ti.API.info(currentTime);
				Ti.API.info(endTime);
				
				if(currentTime.between(startTime, endTime)){
					//Found valid bound
					Ti.API.info("Using slot:"+_$.Session.Ticket.Zone.slots[i].name);
					core.currentSlot = _$.Session.Ticket.Zone.slots[i];
					break;
				}
			}
			*/
			

			//Cancel the current ticket
			$.ticket_step2_cancel.addEventListener('click',function(){
				_$.Tool.openController("navigation");
			});
			
			//Do we have all?
			$.ticket_step2_accept.addEventListener('click',function(){
				Ti.API.info(JSON.stringify(_$.Session.Ticket));
				_$.Tool.openController("ticket_connecting",{url:core.council.coatUrl});
			});
			
		//Add the valid fees
			core.currentSlot = _$.Session.Ticket.Zone.slots[0];
			if(core.currentSlot && Object.keys(core.currentSlot).length>0){
				Ti.API.info("Currentslot existe y tiene:"+JSON.stringify(core.currentSlot));

				for(var i=0;i<core.currentSlot.fee.length;i++){
					slotData = core.currentSlot.fee[i];
					var slot = Alloy.createController('item_step_slot',slotData).getView();
						slot.info = slotData;
					core.slots.push(slot);
					$.ticket_step2_money_wrapper.add(slot);	
					Ti.API.info("Added slot to scroll");
				}
			}else{
				alert("No hay tarifas disponibles válidas en esta zona");
				_$.CurrentController.close();
				_$.Tool.openController("ticket_step1");
			}
	},
	scrollEventListener : function(){
			Ti.API.info("started scrolling fees");
			var offset = $.ticket_step2_money_wrapper.contentOffset.y;
			var full   = $.ticket_step2_money_wrapper.children.length;
			var chheight= 30;
			var coheight= 30*full;
			
			Ti.API.info("If everything is awesome, the height of a single view is:"+chheight);
			Ti.API.info("and, we are currrently focusing on:"+Math.round((offset+chheight/2)/chheight));
			//Ti.API.info("which is :"+JSON.stringify($.ticket_step2_money_wrapper.children[Math.round((offset+chheight/2)/chheight)]));
			var focused = $.ticket_step2_money_wrapper.children[Math.round((offset+chheight)/chheight)];
			
			if(focused && focused.info && focused.info.mins){
				//To avoid trying to use padding views (top/bottom) as selected slot
				for(i=0;i<$.ticket_step2_money_wrapper.children.length;i++){
					var child = $.ticket_step2_money_wrapper.children[i];
					if(i==Math.round((offset+chheight)/chheight)){
						//This acts as "selecting" a fee bracket so we need to update the expiration time to this
						var extra_minutes = child.info.mins;
						
						//Adjust starting time to either slot start (if we are not in slot) or current time if we are
						var d = core.date.parse(_$.Session.Ticket.Zone.slots[0].startTime);
						if(d.compareTo(core.date.now())==-1){
							d = core.date.now();
						}
						
						var format = "yyyy-M-d HH:mm";
						core.times = {start:d.toString(format),end:d.add({ minutes:extra_minutes}).toString(format)};
						_$.Session.Ticket.Zone.Times = core.times;
						
						//Make it readable
						$.ticket_step2_limit_date.text = d.toString('d/M/yyyy');
						$.ticket_step2_limit_time.text = d.toString('HH:mm');
						
						child.children[0].backgroundColor = "#47a3d1";
						_$.Session.Ticket.Zone.Slot = child.info;
						
					}else{
						child.children[0].backgroundColor = "#4D94B8";
					}
				}
			}
			
		},
	
	prepareScreen : function(){
		Ti.UI.setBackgroundImage( '/images/main_bg_pattern.png' );
		$.menu.init("deposit");
		$.open_menu.addEventListener('click', function(){core.toggleMenu();});
		//Ti.App.removeEventListener('openMenu',_$.om);
		//Ti.App.removeEventListener('closeMenu',_$.cm);
		_$.om = Ti.App.addEventListener('openMenu',function(){if(!_$.Tool.menuOpen) core.toggleMenu();});
		_$.cm = Ti.App.addEventListener('closeMenu',function(){if(_$.Tool.menuOpen) core.toggleMenu();});
	},
	toggleMenu : function(){
		_$.Tool.menuOpen=!_$.Tool.menuOpen;
		$.sidebar_container.width	= _$.Tool.menuOpen?    _$.Config.sideBarWidth:"0dp";
		$.content_container.left	= _$.Tool.menuOpen?"-"+_$.Config.sideBarWidth:"0dp";
	}
};
core.init();;