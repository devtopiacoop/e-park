/**
 * This class receives the information for the row data in the arguments
 * passed. If there are no arguments, the row is empty and we are in trouble
 */

var args = arguments[0] || {};
if(!Object.keys(args).length) Ti.API.info("Something went wrong inside `item_zone` we didn't receive data'");
Ti.API.info(JSON.stringify(args));
//Set values
$.zone_name.text = args.name;
	 if(args.name.indexOf("Azul") >= 0)		$.zone.backgroundColor = "#399CC7";
else if(args.name.indexOf("Verde") >= 0)	$.zone.backgroundColor = "#598612";
else if(args.name.indexOf("Roja") >= 0)		$.zone.backgroundColor = "#9D3B34";
else if(args.name.indexOf("Residente") >= 0)$.zone.backgroundColor = "#ABABAB";