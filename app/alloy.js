// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.coords = {"latitude":0,"longitude":0,"accuracy":9999,"startTime":-1};

Alloy.Globals.Profile = {},

Alloy.Globals.Session = {Ticket:{},Vehicle:{},User:{},Cities:[],City:""},

//Ugly, but handles navigation (back button too!)
Alloy.Globals.CurrentController = null;
Alloy.Globals.CurrentControllerName = null;
Alloy.Globals.PreviousController = null;

Alloy.Globals.Colors = {
	BorderInputError : 		"#CC0000",
	BorderInputIdle:		"#E9E9E9",
	BorderInputHighlight:	"#5AAFEE"
},


Alloy.Globals.Strings = {
	LoginError : "No hemos podido acceder a ninguna cuenta con estos datos",
	LoginInvalidIDError: "El identificador no parece ser válido",
	SessionTimeoutError: "La sesión ha caducado, por favor vuelve a acceder",
	
	//Navigation controller
	Navigation:{
		TicketCount : "Tickets en vigor - %d"
	},
	
	//Dialogs
	DialogErrorTitleGeneric: "Error",
	DialogConfirmationTitleVehicleDelete: "Eliminar vehículo",
	DialogConfirmationTitleLogout : "Cierre de sesión",
	DialogConfirmationTitleLanguageChange : "Cambio de idioma",
	DialogConfirmationTitlePasswordChange : "Cambio de contraseña",
	DialogConfirmationDeleteCard : "Borrar tarjeta",
	DialogConfirmationTitleDeleteCard : "¿Seguro que quieres eliminar la tarjeta?",
	DialogConfirmationLanguageChange : "Para garantizar un cambio completo de idioma, debe reiniciar la aplicación",
	DialogConfirmationPasswordChange : "Rellene los campos para cambiar la contraseña",
	DialogConfirmationLogout : "%s, ¿seguro que quieres cerrar la sesión?",
	DialogConfirmationVehicleDelete: "¿Seguro que quieres eliminar el vehículo con matrícula %s?",
	
	DialogConfirm: "OK",
	DialogRestart: "Reiniciar",
	DialogCancel: "Cancelar"
},

Alloy.Globals.Config = {
	"sideBarWidth":"200dp",
	"userToken":-1
},
Alloy.Globals.Tool = {
	openControllers : [],
	setTextLiteralFromKey : function(textfields){
		//TODO
		for(i=0;i<textfields.length;i++){
			if(textfields[i].key && textfields[i].key != ""){
				textfields[i].text = textfields[i].key;
			}
		}
	},
	clearSessionData : function(){
		//Note: if we are in the navigation screen, we can safely assume that any process of purchasing a ticket,
		//adding a car, or whatever on the application has been completed (successfuly or not) and we no longer need
		//the session variables (addedVehicle, newProfileData, or whatever) since they'll be stored either server or
		//locally 
		Alloy.Globals.Session = {Ticket:{},Vehicle:{},User:{},Cities:[],City:{}};
		Ti.API.info("Clearing session data, after creation/deletion of vehicle");
	},
	setTouchListenersForFeedback: function($,TouchableViews){
		/* As a workaround for having state of buttons working properly, list here all 
		 * items that define a pressedbackgroundcolor and update their background accordingly
		 */
		for(var i=0;i<TouchableViews.length;i++){
			child = TouchableViews[i];
			child.addEventListener('touchstart', function(){
				this.originalBackground = this.backgroundColor;
				if(this.backgroundGradient){
					this.originalBackgroundGradient = this.backgroundGradient;
					this.setBackgroundGradient(this.pressedBackgroundGradient);
				}
				this.backgroundColor = this.pressedBackgroundColor;
			});
        	child.addEventListener('touchend', function(){
				this.backgroundColor = this.originalBackground;
				if(this.originalBackgroundGradient){
					this.setBackgroundGradient(this.originalBackgroundGradient);
				}
        	});
		}
	},
	menuOpen: false,
	highlightParents: function(views, activeColor, idleColor){
		for(i=0;i<views.length;i++) this.highlightParent(views[i],activeColor,idleColor);
	},
	highlightParent : function(view, activeColor, idleColor){
		view.addEventListener('focus', function() {
			this.getParent().borderColor = activeColor;
		});
		 
		view.addEventListener('blur', function() {
			this.getParent().borderColor = idleColor;
		});
	},
	testOpenSlideMenu : function(currentController){
		
	},
	openController : function(controllerName,extras){
		if(Titanium.Network.networkType == Titanium.Network.NETWORK_NONE){
		     var alertDialog = Titanium.UI.createAlertDialog({
		              title: '¡ERROR!',
		              message: 'No tienes acceso a internet',
		              buttonNames: ['OK']
		            });
		            alertDialog.show();
		}else{
			var controller = Alloy.createController(controllerName,extras);
			Alloy.Globals.Tool.openControllers.push(controller);
			var view  = controller.getView();
			/*view.addEventListener('swipe', function(e){
			    if(e.direction == "left"){
			    	Ti.App.fireEvent("openMenu");
			    	Ti.API.info("Fired swipe left event, open menu?");
			    }else if(e.direction == "right"){
			    	Ti.App.fireEvent("closeMenu");
			    	Ti.API.info("Fired swipe right event, close menu?");
			    }
			});*/
			view.open();
		}
	},
	openControllerPopUp : function(controllerName, extras){
		Alloy.createController(controllerName,extras).getView().open();
	},
	animation:Ti.UI.createAnimation({curve:2,left:0,right:0,width:Ti.UI.FILL,duration:250})
};
