/**
 * Communication handler with the online services, the logic is divided
 * into modules, located inside /lib/communicator/MODULENAME.js
 * 
 * All services expect a callback function that will receive the data 
 * returned by the server, except from when a server error occured, then
 * we will trigger a generic alert with something on the lines of 
 * "Server error".
 * 
 * This can be changed inside the modules
 */
var Alloy = require('alloy');
_$ = Alloy.Globals;

//Setup
//Load configuration
var core = require("communicator/setup");

//Load app wide services
var d = require("communicator/core").services; //TODO remove
exports.Setup = d.Setup;
exports.GetLiterals = d.GetLiterals;

//Load modules and add services
exports.cards  	 = require("communicator/cards").services;
exports.deposits = require("communicator/deposits").services;
exports.car  	 = require("communicator/cars").services;
exports.user 	 = require("communicator/users").services;
exports.tickets  = require("communicator/tickets").services;
exports.zones    = require("communicator/zones").services;
