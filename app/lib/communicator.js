/**
 * All the files that used to be modules in previous commits of this file
 * are now mashed together here. The culprit is the way Android understands
 * require() and the inability of nesting requires. If anyone can get around
 * that, feel free.
 * 
 * All services expect a callback function that will receive the data 
 * returned by the server, except from when a server error occured, then
 * we will trigger a generic alert with something on the lines of 
 * "Server error".
 * 
 */
var Alloy = require('alloy');
_$ = Alloy.Globals;

//Setup
//Load configuration
var core = {
	baseURL : "http://134.255.184.108/api/",
	GetNewConnection : function(method,url,callback,usesToken,skipCacheTimestamp){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    //Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		request.open(method,this.baseURL+url+(skipCacheTimestamp?"":"?"+new Date().getTime()));
		request.setRequestHeader("Content-Type","application/json");
		
		if(usesToken){
			request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);	
		}
		return request;
	}
};

//Load app wide services

exports.Setup = function(callback){
	Ti.API.info("Fetching configuration");
	core.GetNewConnection("POST","service/configgral",callback,true).send('');
};

//Load modules and add services
//exports.cards  	 = require("communicator/cards").services;
//exports.deposits = require("communicator/deposits").services;
//exports.car  	 = require("communicator/cars").services;
//exports.user 	 = require("communicator/users").services;
//exports.tickets  = require("communicator/tickets").services;
//exports.zones    = require("communicator/zones").services;

exports.cards  	 = 
{
	// SERVICE GET CREDIT CARDS
	// •	URL Service:  http://{{url}}/api/servicesipay/get
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	action: 0
	// •	Respuesta:
	// •	creditCard:
	// •	cardId (Int)
	// •	cardNumber (String)
	// •	titular (String)
	// •	cardCaducity (String)
	// •	pin (Int)
	// •	status (Int):
	// •	-1. No tiene tarjeta
	// •	0. Fallo
	// •	1. OK 
	// Ejemplo Llamada:
	// {
	    // "action": "0"
	// }
	// Ejemplo Respuesta:
	// {
	    // "creditCard": {
	        // "cardId": 255812,
	        // "cardNumber": "************ 0205",
	        // "titular": " Tarjeta 1",
	        // "cardCaducity": "12/17",
	        // "pin": 0
	      // },
	    // "status": 1
	// }

	GetCards : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
			try{
			    var content = JSON.parse(this.responseText);
			    Titanium.API.info(content);
			    if(content.error == undefined) {
			        callback(content);
			    } else {
			        alert(content.error);
			    }
			}catch(e){
				alert("No hemos podido interpretar la respuesta del servidor");
				Ti.API.info("Received invalid JSON from server:");
				Ti.API.info(this.responseText);
			}
		};
		
		var url = core.baseURL+"ServicesIpay/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching credit cards");
		request.send("{'action': '0'}");
	},
	
	// SERVICE DELETE CREDIT CARD
	// •	URL Service: http://{{url}}/api/servicesipay/deletecreditcard
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	idTarjeta (String) Corresponde con el parámetro de respuesta “cardId” del servicio GetCreditCards.
	// •	Respuesta:
	// •	status (Int):
	// •	-1. Autorización denegada
	// •	-2. Baja Rechazada
	// •	-3. Cierre de paquete rechazado.
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "idTarjeta":"255812"
	// }
	// Ejemplo respuesta:
	// {
	  // "status": 1
	// }
	DeleteCard : function(data, callback){
		var url = "servicesipay/deletecreditcard";
		var request = core.GetNewConnection("POST", url, callback, true);
		
		Ti.API.info("Deleting credit card");
		Ti.API.info(JSON.stringify(data));
		request.send(JSON.stringify(data));
	},
	
	// SERVICE CREATE CREDIT CARD
	// •	URL Service:  http://{{url}}/api/servicesipay/createcreditcard
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken : userToken
	// •	Respuesta:
	// •	status (Int)
	// •	url (Sipay envía una URL para realizar el proceso de alta de una tarjeta)
	// 
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url":     "https://sandbox.sipayecommerce.sipay.es:10443/iframe/v1/webviews/tokenizationsstorages?idstorage=44073D8397A18A9CDB7406647616BAF0DC977A02497D4C70D0C433F565E1F62E"
	// }
	CreateCard : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServicesIpay/CreateCreditCard?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Requesting auth for creating a credit card");
		
		request.send("");
	}
},

exports.madrid = {
	/*
	 * Returns an humongous 13k JSON (!)
	 */
	getStreets: function(callback){
		var url = "service/getcalles";
		Ti.API.info("Fetching the big list of streets");
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send('{"version": 47}');
	},
	
	getZones: function(data, callback){
		var url = "serviceparkare/getzonas";
		Ti.API.info("Fetching the zone list for the council");
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
	},
	
	getAuth : function(data,callback){
		var url = "serviceparkare/getauthorizationticket";
		Ti.API.info("Getting permission to park in this area");
		Ti.API.info("Sending:"+JSON.stringify(data));
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
	}
},

exports.deposits = {
	// SERVICE LISTADO RECARGAS
	// •	URL Service: http://{{url}}/api/servicerecarga/get
	// •	Parámetros:
	// •	Cabecera: 
	// •	userToken: userToken
	// •	Respuesta:
	// •	recargas:
	// •	titular (String)
	// •	fecha (String )(Formato: aaaa-mm-dd)
	// •	importe (String)
	// •	saldo (string)
	// •	travel (Int)
	// •	user (String)
	// Ejemplo Respuesta:
	// {
	  // "recargas": [
	    // {
	      // "Titular": "Juano Doe",
	      // "Fecha": "2015-11-12 13:38",
	      // "Importe": "10",
	      // "Saldo": "20",
	      // "Travel": 0
	    // },
	    // {
	      // "Titular": "Juano Doe",
	      // "Fecha": "2015-11-12 13:22",
	      // "Importe": "10",
	      // "Saldo": "10",
	      // "Travel": 0
	    // }
	  // ],
	  // "user": "Juano Doe"
	// }
	Get : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceRecarga/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching deposits");
		request.send("{'action': '0'}");
	},
	
	// SERVICE RECARGA CREDIT CARD
	// •	URL Service: http://{{url}}/api/servicerecarga/create
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	idTarjeta (String) Corresponde con el parámetro de respuesta “cardId” del servicio GetCreditCards.
	// •	importe (Int). Importe en céntimos.
	// •	pin (String). Se refiere a la contraseña de usuario en e-park.
	// •	Respuesta:
	// •	status (Int):
	// •	0. Error
	// •	1. OK
	// •	-1. PIN incorrecto
	// •	-2. Autorización denegada
	// •	-3. Recarga denegada
	// Ejemplo Llamada:
	// {
	    // "idTarjeta":255814,
	    // "importe": 10,
	    // "pin": "uqjrn0q0ark2"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status":1
	// }
	Add : function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceRecarga/Create";
		request.open("POST",url);
		request.setRequestHeader("Content-Type","application/json");
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info(String.format("Using token:%s",Alloy.Globals.Config.userToken));
		Ti.API.info("Making a deposit");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	}
},

exports.car  	 = {
	// SERVICE CREATE CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/create
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	matricula (String)
	// •	marca (String)
	// •	modelo (String)
	// •	idColor (Int)
	// •	Respuesta:
	// •	status (Int):
	// •	-1. Ya existe
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "matricula":"1289ASD",
	    // "marca":"Mercedes",
	    // "modelo":"GLS",
	    // "idColor":4
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Create: function(license, brand, model, idcolor, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert("error:"+content.error);
		    }
		};
	
		var url = core.baseURL+"ServiceCar/Create?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info("Creating vehicle");
		Ti.API.info('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
		request.send('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
	},
	
	// SERVICE LISTADO CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/get
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Respuesta:
	// •	vehicles:
	// •	vehicleRegistration (String)
	// •	vendor (String)
	// •	model (String)
	// •	color (String)
	// Ejemplo Respuesta:
	// {
	  // "vehicles": [
	    // {
	      // "vehicleRegistration": "1289ASD",
	      // "vendor": "Mercedes",
	      // "model": "GLS",
	      // "color": "Naranja"
	    // }
	  // ]
	// }
	Get: function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceCar/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching vehicles");
		request.send("");
	},
	
	// SERVICE DELETE CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/delete
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	matricula (String)
	// •	Respuesta:
	// •	Status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "matricula":"1289ASD",
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Delete: function(vehicle,callback){
		request = Titanium.Network.createHTTPClient();
		var url = core.baseURL+"serviceCar/Delete?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		license = vehicle.vehicleRegistration;
		brand = vehicle.vendor;
		model = vehicle.model;
		idcolor = Ti.App.Properties.getList('color',[]).indexOf(vehicle.color);
		
		request.send('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
		
		Ti.API.info("Deleting car with vehicleRegistration "+vehicle.vehicleRegistration);
		request.send(vehicle);
	}
},

exports.tickets  = {
	// TICKETS EN VIGOR
	// LISTADO DE TICKETS EN VIGOR
		// •	URL Service: http://{{url}}/api/serviceticket/getvigor
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Respuesta:
		// •	ticket
		// •	matricula
		// •	importe
		// •	inicio
		// •	fin
		// •	zoneColor
		// •	zonaNombre
		// •	identificador
		// •	donado
		// •	city
		// •	name
		// •	cityId
		// •	resident
		// •	districts
		// •	social
		// •	anulación
		// •	soporte
		// •	urlExpediente
		// •	zone
		// •	name
		// •	value
		// •	idZone
		// •	slots
		// •	parquímetro
		// •	idBarrio
		// •	template
		// •	code
		// •	codeBarrio
		// •	numeroBarrio
		// •	nombreBarrio
		// •	ocupación
		// •	tabla
		// •	user
		// •	fechaAcual
		// •	saldo
	// Ejemplo Respuesta:
	// {
	  // "ticket": [
	    // {
	      // "Matricula": "1289ASD",
	      // "Importe": "55",
	      // "Inicio": "2015-11-24 13:47",
	      // "Fin": "2015-11-24 14:17",
	      // "ZoneColor": "13559535",
	      // "ZonaNombre": "Azul",
	      // "Identificador": 2757736,
	      // "Donado": 0,
	      // "City": {
	        // "name": "Granada",
	        // "cityId": 18,
	        // "resident": 0,
	        // "districts": [],
	        // "Social": 1,
	        // "Anulacion": 1,
	        // "soporte": "",
	        // "urlExpediente": ""
	      // },
	      // "Zone": {
	        // "name": "Azul",
	        // "value": "-12474674",
	        // "idZone": 28,
	        // "slots": [],
	        // "parquimetro": null,
	        // "idBarrio": 0,
	        // "template": 13,
	        // "code": 0,
	        // "codeBarrio": "0",
	        // "numeroBarrio": 0,
	        // "nombreBarrio": "",
	        // "ocupacion": "Muy Baja"
	      // },
	      // "Tabla": 0
	    // }
	  // ],
	  // "user": "Juan Doe",
	  // "fechaActual": "2015-11-24 13:53",
	  // "saldo": 9205
	// }
	GetCurrent : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/GetVigor?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching active tickets");
		request.send("");
	},
	// OFRECER TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/donation
		// •	Parámetros:
		// •	Body:
		// •	identificador (String)
		// •	Respuesta:
		// •	status:
		// •	0. Error
		// •	1. OK
		// •	url
	// Ejemplo Llamada:
	// {
	    // "identificador" : 2757730
	// }
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url": "http://www.e-park.es/webView/ShareTicket?inicio=2015-11-24 14:56&fin=2015-11-24 15:26&plantilla=13"
	// }
	Donate : function(id, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/Donation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Donating active ticket with id:"+id);
		request.send("{'identificador':"+id+"}");
	},
	// AMPLIAR TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/extend
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Body:
		// •	idTarjeta (Int)
		// •	incremento (Int) Incremento de minutos el ticket
		// •	fechaFin (String) (Formato: aaaa-mm-dd hh:mm)
		// •	identificador (Int)
		// •	pin (String)
		// •	Respuesta:
		// •	status
		// •	0. Error
		// •	1. OK
		// •	-1. Sin Saldo
		// •	-2. Pin Incorrecto
		// •	url
		// •	urlPublicidad
		// •	textoAlarma
	// Ejemplo Llamada:
	// {
	    // "idTarjeta" : 255822,
	    // "incremento" : 30,
	    // "fechaFin" : "2015-11-24 15:56",
	    // "identificador" : 2757738,
	    // "pin" : "12345"
	// }
	// Ejemplo Repsuesta:
	// {
	  // "status": 1,
	  // "url": "https://appdesarrollo.e-park.es/Imagenes/app_publi.jpg",
	  // "urlPublicidad": "https://appdesarrollo.e-park.es/es/novedades.html",
	  // "textoAlarma": "Fin de ticket XXXX.h /ne-park tu parquímetro personal en tu móvil."
	// }
	Extend: function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/Extend?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Extending a ticket");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	// LISTADO DE TICKETS A CAPTURAR
	// Antes de poder capturar un ticket este tiene que estar ofrecido. Para ello hay que ir a la opción de Tickets en Vigor del menú opciones.
			// •	URL Service: http://{{url}}/api/serviceticket/getdonation
			// •	Parámetros:
			// •	Body:
			// •	idCiudad (Int)
			// •	matricula (String)
			// •	Respuesta:
			// •	tickets
			// •	fechaFin
			// •	zona
			// •	ciudad
			// •	color
			// •	fecha
		// Ejemplo Llamada:
		// {
		    // "idCiudad" : 18,
		    // "matricula" : "1289ASD"
		// }
		// Ejemplo Respuesta:
		// {
		  // "tickets": [
		    // {
		      // "Identificador": 33758,
		      // "FechaFin": "2015-11-24 15:26",
		      // "Zona": "Azul",
		      // "Ciudad": "Granada",
		      // "Color": "13559535"
		    // }
		  // ],
		  // "fecha": "2015-57-24 14:57"
		// }
	GetDonatedTickets : function(data,callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/GetDonation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Listing donated tickets for: "+JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	
	// LISTADO DE ZONAS
	GetZones : function(data,callback){
		var url = "servicefee/get";
		var request = core.GetNewConnection("POST",url,callback,true);
		Ti.API.info("Listing zones for: "+JSON.stringify(data));
		request.send(JSON.stringify(data));
	},
	
	// CAPTURAR TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/capturedonation
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Body:
		// •	matricula (String)
		// •	identificador
		// •	Respuesta:
		// •	status
		// •	0. Error
		// •	1. OK
		// •	url
		// •	urlPublicidad
		// •	textoAlarma 	
	// Ejemplo Llamada:
	// {
	    // "matricula" : "1289ASD",
	    // "identificador" : 33758
	// }
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url": "https://appdesarrollo.e-park.es/Imagenes/Imagen2.jpg",
	  // "urlPublicidad": "https://appdesarrollo.e-park.es/es/novedades.html",
	  // "textoAlarma": "Fin de ticket XXXX.h /ne-park tu parquímetro personal en tu móvil."
	// }
	Capture : function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/CaptureDonation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Capturing a ticket");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	
	/*{
    "idZona" : 41,
    "fechaInicio" : "2015-11-24 15:22",
    "fechaFin" : "2015-11-24 15:42",
    "matricula" : "1289ASD",
    "idTarjeta" : 255822,
    "importe" : 35,
    "pin" : "12345"
	}*/
	Create : function(data, callback){
		var url = "serviceticket/pay";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
		
		Ti.API.info("Creating a ticket");
		Ti.API.info(JSON.stringify(data));
	},
	
	HistoricOfMovements : function(callback){
		var url = "servicehistoric/get";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send("");
		
		Ti.API.info("Getting movements");
	}
},

exports.zones = {
	//This returns over 18k lines, beware
	getCoordinates : function(callback){
		var url = "service/getcoordinates?version=0";
		var request = core.GetNewConnection("GET", url, callback, true, true);
		
		Ti.API.info("Fetchin coordinates of zones for map");
		request.send();
	}
},

exports.user = {
	// SERVICE FORGOT PASSWORD
		// •	URL Service: http://{{url}}/api/serviceuser/forgotpwd
		// •	Parámetros:
		// •	Body:
		// •	nif (String)
		// •	Respuesta:
		// •	status (Int):
		// •	0. Usuario existe
		// •	1. OK
		// •	NOTA: este servicio envía al email del usuario un correo con la nueva contraseña generada de manera  automática.
	// Ejemplo Llamada:
	// {
	    // "nif": "05311349m"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	RecoverPassword: function(username,callback){
		var url = "serviceuser/forgotpwd";
		var request = core.GetNewConnection("POST",url,callback,true);
		Ti.API.info('{"NIF":"'+username+'"}');
		request.send('{"NIF":"'+username+'"}');
	},
	
	// SERVICE DATOS USER
		// •	URL Service: http://{{url}}/api/serviceuser/get
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Respuesta:
		// •	user:
		// •	nif (String)
		// •	name (String)
		// •	surName (String)
		// •	phone (Int)
		// •	email (String)
		// •	address (String)
		// •	homeNumber (String)
		// •	flat (String)
		// •	city (String)
		// •	cp (Int)
		// •	vehicles
		// •	vehicleRegistration (String)
		// •	vendor (String)
		// •	model (String)
		// •	color (String)
		// •	userBalance (Int)
		// •	userToken (String)
		// •	mailToken (String)
	// Ejemplo Respuesta:
	// {
	  // "user": {
	    // "nif": "69893895T",
	    // "name": "Sergio",
	    // "surName": "Torres",
	    // "phone": 654789258,
	    // "email": "sergio.torres@tecnilogica.com",
	    // "address": "General Alvarez de Castro",
	    // "homeNumber": "26",
	    // "flat": "bajo",
	    // "city": "madrid",
	    // "cp": 28010,
	    // "vehicles": [
	      // {
	        // "vehicleRegistration": "1289ASD",
	        // "vendor": "Mercedes",
	        // "model": "GLS",
	        // "color": "Naranja"
	      // }
	    // ],
	    // "userBalance": 0,
	    // "userToken": "ddbe3834-d97c-4dc0-9b3d-c345f1c43633",
	    // "mailToken": null
	  // }
	// }
	Profile: function(callback){
		if(Alloy.Globals.Config.userToken == -1){
			_$.Tool.openController('home',$.home);

			Titanium.UI.createAlertDialog({
			    title:_$.Strings.DialogErrorTitleGeneric,
			    message:_$.Strings.SessionTimeoutError
			}).show();
		}
		
		request = Titanium.Network.createHTTPClient();
		var url = core.baseURL+"ServiceUser/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        _$.Profile = content;
		        Ti.API.info("Fetched user profile correctly");
		    } else {
		        alert(content.error);
		    }
		};
		request.send("");
	},
	
	// SERVICE CHANGE PASSWORD
	// •	URL Service: http://{{url}}/api/serviceuser/changepwd
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	pwdOld (String)
	// •	pwdNew (String)
	// •	Respuesta:
	// •	Status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "pwdOld": "password",
	    // "pwdNew": "passwordNew"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	ChangePassword : function(oldpassword, newpassword, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content && content.error == undefined && content.status != "-1") {
		        callback(content);
		    } else {
		        alert("Server error"+content.error);
		    }
		};
		var payload = {
		   PwdOld: oldpassword,
		   PwdNew: newpassword
		};
		var url = core.baseURL+"ServiceUser/ChangePwd?"+new Date().getTime();
		
		request.open("POST",url);
		
		
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info(JSON.stringify(payload));
		request.send(JSON.stringify(payload));
	},
	
	// SERVICE UPDATE USER
	// •	URL Service: http://{{url}}/api/serviceuser/update
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	nombre (String)
	// •	apellidos (String)
	// •	email (String)
	// •	telefono (String)
	// •	calle (String)
	// •	piso (String)
	// •	ciudad (String)
	// •	cp (String)
	// •	numero (String)
	// •	Respuesta:
	// •	status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "nombre": "Juan",
	    // "apellidos": "Doe",
	    // "email": "juan.cortes@tecnilogica.com",
	    // "telefono": 622681988,
	    // "calle": "calle falsa",
	    // "piso": "2",
	    // "ciudad": "madrid",
	    // "cp": "28220",
	    // "numero": "1"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Update : function(user, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined && content.status != "-1") {
		        callback(content);
		    } else {
		        alert("Server error"+content.error);
		    }
		};
		var payload = {
		   nombre: user.name,
		   apellidos: user.surName,
		   email: user.email,
		   telefono: user.phone,
		   calle: user.address,
		   piso: user.flat,
		   ciudad: user.city,
		   cp: user.cp,
		   numero: user.homeNumber
		};
		var url = core.baseURL+"ServiceUser/Update?"+new Date().getTime();
		
		request.open("POST",url);
		
		
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info(JSON.stringify(payload));
		request.send(JSON.stringify(payload));
	},
	
	// SERVICE CREATE USER
	// •	URL Service: http://{{url}}/api/serviceuser/createupdate
	// •	Parámetros: 
	// •	Cabecera:
	// •	UDid: “Web”
	// •	Body:
	// •	userAgent (String): “Web”
	// •	nombre (String)
	// •	apellidos (String)
	// •	telefono (String)
	// •	email (String)
	// •	calle (String)
	// •	nif (String)
	// •	piso (String)
	// •	ciudad (String)
	// •	cp (Int)
	// •	pwd (String)
	// •	pwdMD5 (String)
	// •	numero (Int)
	// •	documentación (Int)(0=NIF,1:CIF,2:NIE)
	// •	Respuesta:
	// •	Status (Int):
	// •	-1. Existe
	// •	0. Error
	// •	1. OK
	// •	userToken (String)
	// Ejemplo Llamada:
	// {
	    // "userAgent": "Web",
	    // "nombre": "Sergio",
	    // "apellidos": "Torres",
	    // "telefono": "654789258",
	    // "email": "sergio.torres@tecnilogica.com",
	    // "calle":"General Alvarez de Castro",
	    // "numero":"26",
	    // "piso":"bajo",
	    // "cp": "28010",
	    // "ciudad": "madrid",
	    // "nif":"69893895T",
	    // "documentacion": 0,
	    // "pwd": "password",
	    // "pwdMD5": ""
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	    // "userToken": “a9027dde-08b9-4e5f-85da-4a2752cba53f”
	// }
	CreateUpdate : function(user, callback){
		var url = "serviceuser/createupdate";
		var request = core.GetNewConnection("POST",url,callback,true);
				
		request.setRequestHeader('UDid', "Web");

		Ti.API.info(JSON.stringify(user));
		request.send(JSON.stringify(user));
	},
	
	// SERVICE LOGIN USER
		// •	URL Service: http://{{url}}/api/serviceuser/login
		// •	Parámetros: 
		// •	Body:
		// •	nif (String)
		// •	pwd (String)
		// •	pushToken (String): “pruebaToken”
		// •	userAgent (String): “Web”
		// •	Respuesta:
		// •	userToken (String)
		// •	status (Int)
		// •	idUser (Int)
	// Ejemplo Llamada:
	// {
	    // "nif": "69893895T",
	    // "pwd": "password",
	    // "pushToken": "pruebaToken",
	    // "userAgent": "Web"
	// }
	// Ejemplo Respuesta:
	// {
	  // "userToken": "ddbe3834-d97c-4dc0-9b3d-c345f1c43633",
	  // "status": 1,
	  // "idUser": 251217
	// }
	Login : function(data,callback){
		var url = "serviceuser/login";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
	},
	
	Logout: function(callback){
		var url = core.baseURL+"ServiceUser/Logout?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Logout called");
		request.send("");
		
		Alloy.Globals.Config.userToken = -1;
		Ti.API.info("Setting token to -1");
	},
};
