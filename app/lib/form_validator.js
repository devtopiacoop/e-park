_$ = Alloy.Globals;

/**
 * Given an array of textfields, return true if all of them have values,
 * highlight the empty ones unless donthighlight is set to true
 * @param {Object} textfields
 */
exports.empty = function(textfields,donthighlight){
	valid = true;
	for(i=0;i<textfields.length;i++){
		if(textfields[i].value == ""){
			if(!donthighlight){
				textfields[i].getParent().borderColor = _$.Colors.BorderInputError;	
			}
			valid = false;
		}else{
			textfields[i].getParent().borderColor = _$.Colors.BorderInputIdle;
		}
	}
	return valid;
};

exports.isValidEmail = function(text){
	
};
