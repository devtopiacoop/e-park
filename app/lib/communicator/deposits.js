//Load configuration
var core = require("communicator/setup");

exports.services = {
	// SERVICE LISTADO RECARGAS
	// •	URL Service: http://{{url}}/api/servicerecarga/get
	// •	Parámetros:
	// •	Cabecera: 
	// •	userToken: userToken
	// •	Respuesta:
	// •	recargas:
	// •	titular (String)
	// •	fecha (String )(Formato: aaaa-mm-dd)
	// •	importe (String)
	// •	saldo (string)
	// •	travel (Int)
	// •	user (String)
	// Ejemplo Respuesta:
	// {
	  // "recargas": [
	    // {
	      // "Titular": "Juano Doe",
	      // "Fecha": "2015-11-12 13:38",
	      // "Importe": "10",
	      // "Saldo": "20",
	      // "Travel": 0
	    // },
	    // {
	      // "Titular": "Juano Doe",
	      // "Fecha": "2015-11-12 13:22",
	      // "Importe": "10",
	      // "Saldo": "10",
	      // "Travel": 0
	    // }
	  // ],
	  // "user": "Juano Doe"
	// }
	Get : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceRecarga/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching deposits");
		request.send("{'action': '0'}");
	},
	
	// SERVICE RECARGA CREDIT CARD
	// •	URL Service: http://{{url}}/api/servicerecarga/create
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	idTarjeta (String) Corresponde con el parámetro de respuesta “cardId” del servicio GetCreditCards.
	// •	importe (Int). Importe en céntimos.
	// •	pin (String). Se refiere a la contraseña de usuario en e-park.
	// •	Respuesta:
	// •	status (Int):
	// •	0. Error
	// •	1. OK
	// •	-1. PIN incorrecto
	// •	-2. Autorización denegada
	// •	-3. Recarga denegada
	// Ejemplo Llamada:
	// {
	    // "idTarjeta":255814,
	    // "importe": 10,
	    // "pin": "uqjrn0q0ark2"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status":1
	// }
	Add : function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceRecarga/Create";
		request.open("POST",url);
		request.setRequestHeader("Content-Type","application/json");
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info(String.format("Using token:%s",Alloy.Globals.Config.userToken));
		Ti.API.info("Making a deposit");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	}
};