//Load configuration
var core = require("communicator/setup");

exports.services = {
	// SERVICE FORGOT PASSWORD
		// •	URL Service: http://{{url}}/api/serviceuser/forgotpwd
		// •	Parámetros:
		// •	Body:
		// •	nif (String)
		// •	Respuesta:
		// •	status (Int):
		// •	0. Usuario existe
		// •	1. OK
		// •	NOTA: este servicio envía al email del usuario un correo con la nueva contraseña generada de manera  automática.
	// Ejemplo Llamada:
	// {
	    // "nif": "05311349m"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	RecoverPassword: function(username,callback){
		var url = "serviceuser/forgotpwd";
		var request = core.GetNewConnection("POST",url,callback,true);
		Ti.API.info('{"NIF":"'+username+'"}');
		request.send('{"NIF":"'+username+'"}');
	},
	
	// SERVICE DATOS USER
		// •	URL Service: http://{{url}}/api/serviceuser/get
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Respuesta:
		// •	user:
		// •	nif (String)
		// •	name (String)
		// •	surName (String)
		// •	phone (Int)
		// •	email (String)
		// •	address (String)
		// •	homeNumber (String)
		// •	flat (String)
		// •	city (String)
		// •	cp (Int)
		// •	vehicles
		// •	vehicleRegistration (String)
		// •	vendor (String)
		// •	model (String)
		// •	color (String)
		// •	userBalance (Int)
		// •	userToken (String)
		// •	mailToken (String)
	// Ejemplo Respuesta:
	// {
	  // "user": {
	    // "nif": "69893895T",
	    // "name": "Sergio",
	    // "surName": "Torres",
	    // "phone": 654789258,
	    // "email": "sergio.torres@tecnilogica.com",
	    // "address": "General Alvarez de Castro",
	    // "homeNumber": "26",
	    // "flat": "bajo",
	    // "city": "madrid",
	    // "cp": 28010,
	    // "vehicles": [
	      // {
	        // "vehicleRegistration": "1289ASD",
	        // "vendor": "Mercedes",
	        // "model": "GLS",
	        // "color": "Naranja"
	      // }
	    // ],
	    // "userBalance": 0,
	    // "userToken": "ddbe3834-d97c-4dc0-9b3d-c345f1c43633",
	    // "mailToken": null
	  // }
	// }
	Profile: function(callback){
		if(Alloy.Globals.Config.userToken == -1){
			_$.Tool.openController('home',$.home);

			Titanium.UI.createAlertDialog({
			    title:_$.Strings.DialogErrorTitleGeneric,
			    message:_$.Strings.SessionTimeoutError
			}).show();
		}
		
		request = Titanium.Network.createHTTPClient();
		var url = core.baseURL+"ServiceUser/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        _$.Profile = content;
		        Ti.API.info("Fetched user profile correctly");
		    } else {
		        alert(content.error);
		    }
		};
		request.send("");
	},
	
	// SERVICE CHANGE PASSWORD
	// •	URL Service: http://{{url}}/api/serviceuser/changepwd
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	pwdOld (String)
	// •	pwdNew (String)
	// •	Respuesta:
	// •	Status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "pwdOld": "password",
	    // "pwdNew": "passwordNew"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	ChangePassword : function(oldpassword, newpassword, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content && content.error == undefined && content.status != "-1") {
		        callback(content);
		    } else {
		        alert("Server error"+content.error);
		    }
		};
		var payload = {
		   PwdOld: oldpassword,
		   PwdNew: newpassword
		};
		var url = core.baseURL+"ServiceUser/ChangePwd?"+new Date().getTime();
		
		request.open("POST",url);
		
		
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info(JSON.stringify(payload));
		request.send(JSON.stringify(payload));
	},
	
	// SERVICE UPDATE USER
	// •	URL Service: http://{{url}}/api/serviceuser/update
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	nombre (String)
	// •	apellidos (String)
	// •	email (String)
	// •	telefono (String)
	// •	calle (String)
	// •	piso (String)
	// •	ciudad (String)
	// •	cp (String)
	// •	numero (String)
	// •	Respuesta:
	// •	status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "nombre": "Juan",
	    // "apellidos": "Doe",
	    // "email": "juan.cortes@tecnilogica.com",
	    // "telefono": 622681988,
	    // "calle": "calle falsa",
	    // "piso": "2",
	    // "ciudad": "madrid",
	    // "cp": "28220",
	    // "numero": "1"
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Update : function(user, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined && content.status != "-1") {
		        callback(content);
		    } else {
		        alert("Server error"+content.error);
		    }
		};
		var payload = {
		   nombre: user.name,
		   apellidos: user.surName,
		   email: user.email,
		   telefono: user.phone,
		   calle: user.address,
		   piso: user.flat,
		   ciudad: user.city,
		   cp: user.cp,
		   numero: user.homeNumber
		};
		var url = core.baseURL+"ServiceUser/Update?"+new Date().getTime();
		
		request.open("POST",url);
		
		
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info(JSON.stringify(payload));
		request.send(JSON.stringify(payload));
	},
	
	// SERVICE CREATE USER
	// •	URL Service: http://{{url}}/api/serviceuser/createupdate
	// •	Parámetros: 
	// •	Cabecera:
	// •	UDid: “Web”
	// •	Body:
	// •	userAgent (String): “Web”
	// •	nombre (String)
	// •	apellidos (String)
	// •	telefono (String)
	// •	email (String)
	// •	calle (String)
	// •	nif (String)
	// •	piso (String)
	// •	ciudad (String)
	// •	cp (Int)
	// •	pwd (String)
	// •	pwdMD5 (String)
	// •	numero (Int)
	// •	documentación (Int)(0=NIF,1:CIF,2:NIE)
	// •	Respuesta:
	// •	Status (Int):
	// •	-1. Existe
	// •	0. Error
	// •	1. OK
	// •	userToken (String)
	// Ejemplo Llamada:
	// {
	    // "userAgent": "Web",
	    // "nombre": "Sergio",
	    // "apellidos": "Torres",
	    // "telefono": "654789258",
	    // "email": "sergio.torres@tecnilogica.com",
	    // "calle":"General Alvarez de Castro",
	    // "numero":"26",
	    // "piso":"bajo",
	    // "cp": "28010",
	    // "ciudad": "madrid",
	    // "nif":"69893895T",
	    // "documentacion": 0,
	    // "pwd": "password",
	    // "pwdMD5": ""
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	    // "userToken": “a9027dde-08b9-4e5f-85da-4a2752cba53f”
	// }
	CreateUpdate : function(user, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined && content.status != "-1") {
		        callback(content);
		    } else {
		        alert("Server error"+content.error);
		    }
		};
	
		var url = core.baseURL+"ServiceUser/CreateUpdate?"+new Date().getTime();
		
		request.open("POST",url);
		request.setRequestHeader('UDid', "Web");
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info(JSON.stringify(user));
		request.send(JSON.stringify(user));
	},
	
	// SERVICE LOGIN USER
		// •	URL Service: http://{{url}}/api/serviceuser/login
		// •	Parámetros: 
		// •	Body:
		// •	nif (String)
		// •	pwd (String)
		// •	pushToken (String): “pruebaToken”
		// •	userAgent (String): “Web”
		// •	Respuesta:
		// •	userToken (String)
		// •	status (Int)
		// •	idUser (Int)
	// Ejemplo Llamada:
	// {
	    // "nif": "69893895T",
	    // "pwd": "password",
	    // "pushToken": "pruebaToken",
	    // "userAgent": "Web"
	// }
	// Ejemplo Respuesta:
	// {
	  // "userToken": "ddbe3834-d97c-4dc0-9b3d-c345f1c43633",
	  // "status": 1,
	  // "idUser": 251217
	// }
	Login : function(data,callback){
		var url = "serviceuser/login";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
	},
	
	Logout: function(callback){
		var url = core.baseURL+"ServiceUser/Logout?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Logout called");
		request.send("");
		
		Alloy.Globals.Config.userToken = -1;
		Ti.API.info("Setting token to -1");
	},
};