exports = {
	baseURL : "http://134.255.184.108/api/",
	GetNewConnection : function(method,url,callback,usesToken,skipCacheTimestamp){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    //Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		request.open(method,this.baseURL+url+(skipCacheTimestamp?"":"?"+new Date().getTime()));
		request.setRequestHeader("Content-Type","application/json");
		
		if(usesToken){
			request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);	
		}
		return request;
	}
};