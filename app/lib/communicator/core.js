//Load configuration
var core = require("communicator/setup");

exports.services = {
	Setup: function(callback){
		core.GetNewConnection("POST","service/configgral",callback,false).send('');
		Ti.API.info("Fetching configuration");
	},
	
	GetLiterals : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"Service/GetCopys?"+new Date().getTime();
		request.open("POST",url);
		
		request.setRequestHeader('language', Ti.App.Properties.getString("language","es_ES"));
		request.send("{'version':12}");
		Ti.API.info(JSON.stringify(request));
	}
};