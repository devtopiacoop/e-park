//Load configuration
var core = require("communicator/setup");

exports.services = {
	//This returns over 18k lines, beware
	getCoordinates : function(callback){
		var url = "service/getcoordinates?version=0";
		var request = core.GetNewConnection("GET", url, callback, true, true);
		
		Ti.API.info("Fetchin coordinates of zones for map");
		request.send();
	}
};