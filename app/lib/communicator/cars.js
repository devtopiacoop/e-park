//Load configuration
var core = require("communicator/setup");

exports.services = {
	// SERVICE CREATE CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/create
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	matricula (String)
	// •	marca (String)
	// •	modelo (String)
	// •	idColor (Int)
	// •	Respuesta:
	// •	status (Int):
	// •	-1. Ya existe
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "matricula":"1289ASD",
	    // "marca":"Mercedes",
	    // "modelo":"GLS",
	    // "idColor":4
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Create: function(license, brand, model, idcolor, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert("error:"+content.error);
		    }
		};
	
		var url = core.baseURL+"ServiceCar/Create?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		Ti.API.info("Creating vehicle");
		Ti.API.info('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
		request.send('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
	},
	
	// SERVICE LISTADO CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/get
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Respuesta:
	// •	vehicles:
	// •	vehicleRegistration (String)
	// •	vendor (String)
	// •	model (String)
	// •	color (String)
	// Ejemplo Respuesta:
	// {
	  // "vehicles": [
	    // {
	      // "vehicleRegistration": "1289ASD",
	      // "vendor": "Mercedes",
	      // "model": "GLS",
	      // "color": "Naranja"
	    // }
	  // ]
	// }
	Get: function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceCar/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching vehicles");
		request.send("");
	},
	
	// SERVICE DELETE CARS USER
	// •	URL Service: http://{{url}}/api/servicecar/delete
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	matricula (String)
	// •	Respuesta:
	// •	Status (Int):
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "matricula":"1289ASD",
	// }
	// Ejemplo Respuesta:
	// {
	    // "status": 1
	// }
	Delete: function(vehicle,callback){
		request = Titanium.Network.createHTTPClient();
		var url = core.baseURL+"serviceCar/Delete?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
		
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		license = vehicle.vehicleRegistration;
		brand = vehicle.vendor;
		model = vehicle.model;
		idcolor = Ti.App.Properties.getList('color',[]).indexOf(vehicle.color);
		
		request.send('{"Matricula":"'+license+'","Marca":"'+brand+'","Modelo":"'+model+'","IdColor":'+idcolor+'}');
		
		Ti.API.info("Deleting car with vehicleRegistration "+vehicle.vehicleRegistration);
		request.send(vehicle);
	}
};