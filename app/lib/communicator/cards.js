//Load configuration
var core = require("communicator/setup");

exports.services = {
	// SERVICE GET CREDIT CARDS
	// •	URL Service:  http://{{url}}/api/servicesipay/get
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	action: 0
	// •	Respuesta:
	// •	creditCard:
	// •	cardId (Int)
	// •	cardNumber (String)
	// •	titular (String)
	// •	cardCaducity (String)
	// •	pin (Int)
	// •	status (Int):
	// •	-1. No tiene tarjeta
	// •	0. Fallo
	// •	1. OK 
	// Ejemplo Llamada:
	// {
	    // "action": "0"
	// }
	// Ejemplo Respuesta:
	// {
	    // "creditCard": {
	        // "cardId": 255812,
	        // "cardNumber": "************ 0205",
	        // "titular": " Tarjeta 1",
	        // "cardCaducity": "12/17",
	        // "pin": 0
	      // },
	    // "status": 1
	// }

	GetCards : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServicesIpay/Get?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching credit cards");
		request.send("{'action': '0'}");
	},
	
	// SERVICE DELETE CREDIT CARD
	// •	URL Service: http://{{url}}/api/servicesipay/deletecreditcard
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken: userToken
	// •	Body:
	// •	idTarjeta (String) Corresponde con el parámetro de respuesta “cardId” del servicio GetCreditCards.
	// •	Respuesta:
	// •	status (Int):
	// •	-1. Autorización denegada
	// •	-2. Baja Rechazada
	// •	-3. Cierre de paquete rechazado.
	// •	0. Error
	// •	1. OK
	// Ejemplo Llamada:
	// {
	    // "idTarjeta":"255812"
	// }
	// Ejemplo respuesta:
	// {
	  // "status": 1
	// }
	DeleteCard : function(data, callback){
		var url = "servicesipay/deletecreditcard";
		var request = core.GetNewConnection("POST", url, callback, true);
		
		Ti.API.info("Deleting credit card");
		Ti.API.info(JSON.stringify(data));
		request.send(JSON.stringify(data));
	},
	
	// SERVICE CREATE CREDIT CARD
	// •	URL Service:  http://{{url}}/api/servicesipay/createcreditcard
	// •	Parámetros:
	// •	Cabecera:
	// •	userToken : userToken
	// •	Respuesta:
	// •	status (Int)
	// •	url (Sipay envía una URL para realizar el proceso de alta de una tarjeta)
	// 
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url":     "https://sandbox.sipayecommerce.sipay.es:10443/iframe/v1/webviews/tokenizationsstorages?idstorage=44073D8397A18A9CDB7406647616BAF0DC977A02497D4C70D0C433F565E1F62E"
	// }
	CreateCard : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServicesIpay/CreateCreditCard?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Requesting auth for creating a credit card");
		
		request.send("");
	}
};