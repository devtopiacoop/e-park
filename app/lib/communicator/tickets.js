//Load configuration
var core = require("communicator/setup");


exports.services = {
	// TICKETS EN VIGOR
	// LISTADO DE TICKETS EN VIGOR
		// •	URL Service: http://{{url}}/api/serviceticket/getvigor
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Respuesta:
		// •	ticket
		// •	matricula
		// •	importe
		// •	inicio
		// •	fin
		// •	zoneColor
		// •	zonaNombre
		// •	identificador
		// •	donado
		// •	city
		// •	name
		// •	cityId
		// •	resident
		// •	districts
		// •	social
		// •	anulación
		// •	soporte
		// •	urlExpediente
		// •	zone
		// •	name
		// •	value
		// •	idZone
		// •	slots
		// •	parquímetro
		// •	idBarrio
		// •	template
		// •	code
		// •	codeBarrio
		// •	numeroBarrio
		// •	nombreBarrio
		// •	ocupación
		// •	tabla
		// •	user
		// •	fechaAcual
		// •	saldo
	// Ejemplo Respuesta:
	// {
	  // "ticket": [
	    // {
	      // "Matricula": "1289ASD",
	      // "Importe": "55",
	      // "Inicio": "2015-11-24 13:47",
	      // "Fin": "2015-11-24 14:17",
	      // "ZoneColor": "13559535",
	      // "ZonaNombre": "Azul",
	      // "Identificador": 2757736,
	      // "Donado": 0,
	      // "City": {
	        // "name": "Granada",
	        // "cityId": 18,
	        // "resident": 0,
	        // "districts": [],
	        // "Social": 1,
	        // "Anulacion": 1,
	        // "soporte": "",
	        // "urlExpediente": ""
	      // },
	      // "Zone": {
	        // "name": "Azul",
	        // "value": "-12474674",
	        // "idZone": 28,
	        // "slots": [],
	        // "parquimetro": null,
	        // "idBarrio": 0,
	        // "template": 13,
	        // "code": 0,
	        // "codeBarrio": "0",
	        // "numeroBarrio": 0,
	        // "nombreBarrio": "",
	        // "ocupacion": "Muy Baja"
	      // },
	      // "Tabla": 0
	    // }
	  // ],
	  // "user": "Juan Doe",
	  // "fechaActual": "2015-11-24 13:53",
	  // "saldo": 9205
	// }
	GetCurrent : function(callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/GetVigor?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Fetching active tickets");
		request.send("");
	},
	// OFRECER TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/donation
		// •	Parámetros:
		// •	Body:
		// •	identificador (String)
		// •	Respuesta:
		// •	status:
		// •	0. Error
		// •	1. OK
		// •	url
	// Ejemplo Llamada:
	// {
	    // "identificador" : 2757730
	// }
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url": "http://www.e-park.es/webView/ShareTicket?inicio=2015-11-24 14:56&fin=2015-11-24 15:26&plantilla=13"
	// }
	Donate : function(id, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/Donation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Donating active ticket with id:"+id);
		request.send("{'identificador':"+id+"}");
	},
	// AMPLIAR TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/extend
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Body:
		// •	idTarjeta (Int)
		// •	incremento (Int) Incremento de minutos el ticket
		// •	fechaFin (String) (Formato: aaaa-mm-dd hh:mm)
		// •	identificador (Int)
		// •	pin (String)
		// •	Respuesta:
		// •	status
		// •	0. Error
		// •	1. OK
		// •	-1. Sin Saldo
		// •	-2. Pin Incorrecto
		// •	url
		// •	urlPublicidad
		// •	textoAlarma
	// Ejemplo Llamada:
	// {
	    // "idTarjeta" : 255822,
	    // "incremento" : 30,
	    // "fechaFin" : "2015-11-24 15:56",
	    // "identificador" : 2757738,
	    // "pin" : "12345"
	// }
	// Ejemplo Repsuesta:
	// {
	  // "status": 1,
	  // "url": "https://appdesarrollo.e-park.es/Imagenes/app_publi.jpg",
	  // "urlPublicidad": "https://appdesarrollo.e-park.es/es/novedades.html",
	  // "textoAlarma": "Fin de ticket XXXX.h /ne-park tu parquímetro personal en tu móvil."
	// }
	Extend: function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/Extend?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Extending a ticket");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	// LISTADO DE TICKETS A CAPTURAR
	// Antes de poder capturar un ticket este tiene que estar ofrecido. Para ello hay que ir a la opción de Tickets en Vigor del menú opciones.
			// •	URL Service: http://{{url}}/api/serviceticket/getdonation
			// •	Parámetros:
			// •	Body:
			// •	idCiudad (Int)
			// •	matricula (String)
			// •	Respuesta:
			// •	tickets
			// •	fechaFin
			// •	zona
			// •	ciudad
			// •	color
			// •	fecha
		// Ejemplo Llamada:
		// {
		    // "idCiudad" : 18,
		    // "matricula" : "1289ASD"
		// }
		// Ejemplo Respuesta:
		// {
		  // "tickets": [
		    // {
		      // "Identificador": 33758,
		      // "FechaFin": "2015-11-24 15:26",
		      // "Zona": "Azul",
		      // "Ciudad": "Granada",
		      // "Color": "13559535"
		    // }
		  // ],
		  // "fecha": "2015-57-24 14:57"
		// }
	GetDonatedTickets : function(data,callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/GetDonation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Listing donated tickets for: "+JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	
	// LISTADO DE ZONAS
	GetZones : function(data,callback){
		var url = "servicefee/get";
		var request = core.GetNewConnection("POST",url,callback,true);
		Ti.API.info("Listing zones for: "+JSON.stringify(data));
		request.send(JSON.stringify(data));
	},
	
	// CAPTURAR TICKET
		// •	URL Service: http://{{url}}/api/serviceticket/capturedonation
		// •	Parámetros:
		// •	Cabecera:
		// •	userToken: userToken
		// •	Body:
		// •	matricula (String)
		// •	identificador
		// •	Respuesta:
		// •	status
		// •	0. Error
		// •	1. OK
		// •	url
		// •	urlPublicidad
		// •	textoAlarma 	
	// Ejemplo Llamada:
	// {
	    // "matricula" : "1289ASD",
	    // "identificador" : 33758
	// }
	// Ejemplo Respuesta:
	// {
	  // "status": 1,
	  // "url": "https://appdesarrollo.e-park.es/Imagenes/Imagen2.jpg",
	  // "urlPublicidad": "https://appdesarrollo.e-park.es/es/novedades.html",
	  // "textoAlarma": "Fin de ticket XXXX.h /ne-park tu parquímetro personal en tu móvil."
	// }
	Capture : function(data, callback){
		var request = Titanium.Network.createHTTPClient();
		request.onload = function(e){
		    var content = JSON.parse(this.responseText);
		    Titanium.API.info(content);
		    if(content.error == undefined) {
		        callback(content);
		    } else {
		        alert(content.error);
		    }
		};
		
		var url = core.baseURL+"ServiceTicket/CaptureDonation?"+new Date().getTime();
		request.open("POST",url);
		request.setRequestHeader('userToken', Alloy.Globals.Config.userToken);
		
		Ti.API.info("Capturing a ticket");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	
	/*{
    "idZona" : 41,
    "fechaInicio" : "2015-11-24 15:22",
    "fechaFin" : "2015-11-24 15:42",
    "matricula" : "1289ASD",
    "idTarjeta" : 255822,
    "importe" : 35,
    "pin" : "12345"
	}*/
	Create : function(data, callback){
		var url = "serviceticket/pay";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
		
		Ti.API.info("Creating a ticket");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	},
	
	HistoricOfMovements : function(callback){
		var url = "servicehistoric/get";
		var request = core.GetNewConnection("POST",url,callback,true);
		request.send(JSON.stringify(data));
		
		Ti.API.info("Getting movements");
		Ti.API.info(JSON.stringify(data));
		
		request.send(JSON.stringify(data));
	}
};